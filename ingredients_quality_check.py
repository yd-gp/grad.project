"""
    Copyright (C) 2021  Y.Dogay [dogayyagmur@gmail.com]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import sqlite3
import os
from molecules_dframe import read_data


def sql_data(file):
    """
    get NER values from sql data and put into a list
    """
    # Create a SQL connection to our SQLite database
    con = sqlite3.connect(file)

    cur = con.cursor()

    database_list = []
    # The result of a "cursor.execute" can be iterated over by row
    for row in cur.execute('SELECT * FROM ner;'):
        #     print(row[2])
        database_list.append(row[2])
    # Be sure to close the connection
    con.close()
    return database_list


def lower_and_put_into_list(data):
    """
    lowercase
    """
    # read FDB data in a dataframe

    data_2 = data.copy()
    data_2['alias'] = data_2['alias'].str.lower()
    fbd_data = data_2.copy()
    fdb_alias_list = []
    for i in fbd_data['alias']:
        fdb_alias_list.append(i)
    #     print(i.lower())
    return fdb_alias_list, fbd_data


def match_ingredients(list1, list2):
    """
    match ingredients
    """
    # execute the ingredients that match with both databases
    return set(list1) & set(list2)


def mismatch_ingredients(list1, list2):
    """
    mismatch ingredients
    """
    # ingredients that mismatch
    mismatch = [element for element in list2 if element not in list1]
    return mismatch


def create_new_dataframe(list1, list2, fdb_data):
    """
    create new dataframe from FBD data with only taking mathec ingredients,
    dropping rows that contains information about tobacco and any ingredient under dish category
    """
    ingredients = list(set(list1) & set(list2))
    new_fdb_data = fdb_data[fdb_data.alias.isin(ingredients)]
    # reset index
    new_fdb_data = new_fdb_data[new_fdb_data.alias != 'tobacco']
    new_fdb_data = new_fdb_data[new_fdb_data.category_readable != 'Dish']
    new_fdb_data.reset_index(drop=True, inplace=True)
    return new_fdb_data


def save_json(data, path):
    """
    save new created dataframe as json
    """
    data.to_json(path + '/' + 'data_after_qc.json', orient='records')


def main():
    """
    path to result_FDB json file, and recipe sql data
    reading json file, and sql data
    calling functions to run the script
    """
    path = os.path.abspath('all_files')
    file = path + '/' + "result_FDB.json"
    data = read_data(file)
    print(data.columns)
    sql_file = 'recipe.db'
    database_list = sql_data(sql_file)
    fbd_alias_list = lower_and_put_into_list(data)[0]
    new_data = lower_and_put_into_list(data)[1]
    print(len(fbd_alias_list))
    matched_list = match_ingredients(database_list, fbd_alias_list)
    print(matched_list)
    print(len(matched_list))
    mismatch_list = mismatch_ingredients(database_list, fbd_alias_list)
    print(mismatch_list)
    print(len(mismatch_list))
    new_df = create_new_dataframe(database_list, fbd_alias_list, new_data)
    print(new_df.alias)
    print(new_df)
    print(new_df[new_df['alias'] == 'tobacco'])
    save_json(new_df, path)


if __name__ == '__main__':
    main()
