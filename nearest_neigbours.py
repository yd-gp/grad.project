"""
    Copyright (C) 2021  Y.Dogay [dogayyagmur@gmail.com]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import os
from bokeh.models import LabelSet, Range1d, Panel, Tabs, ColumnDataSource, HoverTool, Legend, Title
from bokeh.plotting import figure, output_file, show
import pandas as pd
from molecules_dframe import read_data
from dimension_reduction_baseline import category_colors
from pearson_correlation import create_data_frame, s_compounds, sorted_df, percentage
from eucledian_distace import select_rows, create_lists_for_new_frame


def merge_frames(data, data_2):
    """
    merging baseline or graph data with dimensionaly reducted data
    """
    data = data[['alias', 'category_readable', 'dimension1', 'dimension2']]
    data_2 = data_2[['alias', 'pubchem_id', 'common_name', ]]
    data_3 = pd.merge(data_2, data, on='alias')
    return data_3


def preprocess_data_for_distance_calculation(r_dimension_results):
    """
    reindexing frame
    putting beef, chicken and pork to the top of the frame
    """
    beef = r_dimension_results[r_dimension_results['alias'] == 'beef']
    chicken = r_dimension_results[r_dimension_results['alias'] == 'chicken']
    pork = r_dimension_results[r_dimension_results['alias'] == 'pork']
    p_data = r_dimension_results[r_dimension_results['category_readable'] == 'Plant_Based']
    p_data.reset_index(drop=True, inplace=True)
    others = [chicken, pork, p_data]
    for other in others:
        beef = beef.append(other, ignore_index=True)
    n_data = beef.copy()
    return n_data


def do_distance(each_file, full_data, path):
    """
    calculating distance between objects
    """
    file = path + '/' + each_file

    data_1 = read_data(file)

    frame = merge_frames(data_1, full_data)
    print(frame)
    frame_2 = preprocess_data_for_distance_calculation(frame)
    d_lists = create_lists_for_new_frame(frame_2)
    columns = ['Ingredient_1', 'Ingredient_2', 'Distance', 'Category', 'I_1_Pubchem_id',
               'I_2_Pubchem_id', 'I_1_Common_name', 'I_2_Common_name']
    f_frame = create_data_frame(d_lists, columns)
    p_column = 'Distance'
    boolean = True
    sorted_frame = sorted_df(f_frame, p_column, boolean)
    selected_frame = select_rows(sorted_frame, number=100)
    c_frame = s_compounds(selected_frame)
    print(c_frame)
    c_frame = category_colors(c_frame, column='Category')
    print(c_frame)
    last_frame = percentage(c_frame)
    print(last_frame)
    return last_frame


def n_plot(data, data_2, path, file_name):
    """
    plotting nearest neighbours for beef, chicken and pork
    assigning smaller text size to higher number of neighbours
    mouseover points; shows ingredient name, x and y value, category
    """
    for item in data['Ingredient_1'].unique():
        new_item_data = data[data['Ingredient_1'] == item]
        new_item_data = new_item_data[new_item_data['Category'] != 'Meat']
        new_item_data.reset_index(drop=True, inplace=True)
        print(new_item_data)
        tabs = []
        numbers = [3, 5, 10, 15, 20, 30, 40, 50]
        tex_size = [8, 8, 7, 7, 5, 5, 5, 5]
        dictionary = dict(zip(numbers, tex_size))
        # print(item)
        for num, t_size in dictionary.items():
            name = list(new_item_data.iloc[:int(num), 1:2]['Ingredient_2'])
            name.append(str(item))
            sub_plot = data_2[data_2.alias.isin(name) == True]
            sub_plot.reset_index(drop=True, inplace=True)
            # return sub_plot
            source = ColumnDataSource(data=dict(x=sub_plot.dimension1, y=sub_plot.dimension2,
                                                desc=sub_plot.alias,
                                                category=sub_plot.category_readable,
                                                coluor=sub_plot.color))
            x_min = min(sub_plot.dimension1)
            y_min = min(sub_plot.dimension2)
            x_max = max(sub_plot.dimension1)
            y_max = max(sub_plot.dimension2)
            x_range = Range1d(x_min - 0.1, x_max + .1)
            y_range = Range1d(y_min - 0.1, y_max + 0.1)

            hover = HoverTool(tooltips=[
                ("category", "@category"),
                ("(x,y)", "(@x, @y)"),
                ('name', '@desc'),
            ])
            plot = figure(plot_width=400, plot_height=400,
                       tools=[hover, 'pan', 'wheel_zoom', 'box_zoom', 'zoom_out', 'save'],
                       x_axis_label="Dimension 1",
                       y_axis_label="Dimension 2", background_fill_color="grey",
                          background_fill_alpha=0.1,
                       x_range=x_range, y_range=y_range)
            plot.axis.axis_label_text_font_style = 'normal'
            plot.add_layout(Legend(), 'below')
            plot.scatter('x', 'y', size=5, source=source, alpha=0.5, fill_alpha=0.5, color='coluor',
                      legend_group='category')
            labels = LabelSet(x='x', y='y', text='desc',
                              x_offset=-5, y_offset=2, source=source, render_mode='canvas',
                              text_font_size=str(t_size) + "pt")
            # p.legend.location = "bottom_right"
            plot.add_layout(
                Title(text=" Based on KPCA", text_font_size="9pt", align="center",
                      text_line_height=0.5),
                'above')
            plot.add_layout(Title(text="Nearest Plant-Based Ingredients to " + str(item.upper()),
                                  text_font_size="9pt",
                               align="center",
                               text_line_height=0.5), 'above')
            plot.legend.glyph_height = 7
            plot.legend.title = 'Ingredient Categories'
            plot.legend.title_text_font_style = "bold"
            plot.legend.title_text_font_size = "9px"
            # increasing the glyph width
            plot.legend.glyph_width = 20

            # Increasing the glyph's label height
            plot.legend.label_height = 10

            # Increasing the glyph's label height
            plot.legend.label_width = 10

            # change appearance of legend text
            plot.legend.label_text_font = "times"
            plot.legend.label_text_font_style = "italic"
            plot.legend.label_text_color = "black"
            plot.legend.label_text_font_size = "7pt"
            plot.legend.orientation = 'horizontal'
            # change border and background of legend
            plot.legend.border_line_width = 0.1
            plot.legend.border_line_color = "black"
            plot.legend.border_line_alpha = 0.8
            plot.legend.background_fill_color = "white"
            plot.legend.background_fill_alpha = 0.1

            plot.add_layout(labels)

            tab = Panel(child=plot, title=str(num) + ' Nearest Data Points')
            tabs.append(tab)
        tabs_object = Tabs(tabs=tabs)
        # tabs_object = Tabs(tabs=plots)
        # path = os.path.abspath('all_files/plots')
        # output_file(str(path) + '/' + str(name) + '.html')
        # save(tabs_object)
        # show(tabs_object)
        output_file(str(path) + '/' + str(item.upper()) + str(file_name) + '.html')
        show(tabs_object)


def main():
    """
    calling functions to plot nearest points to beef, chicken and pork based on
    2 dimension data
    """
    baseline_path = os.path.abspath('all_files/baseline/dimension_reduction')
    node2vec_path = os.path.abspath('all_files/gdb/dimension_reduction')
    baseline_files = os.listdir(baseline_path)
    node2vec_files = os.listdir(node2vec_path)
    both_files = baseline_files + node2vec_files
    path_2 = os.path.abspath('all_files/')
    file_2 = path_2 + '/' + "data_after_qc.json"
    data_2 = read_data(file_2)
    # print(both_files)
    for each_file in both_files:
        if each_file.endswith('baseline.json') and each_file.startswith('kernel_pca'):
            print(each_file)
            data_1 = read_data(baseline_path+'/'+each_file)
            name = each_file.split('.')[-2]
            frame = do_distance(each_file, data_2, baseline_path)
            print(frame)
            plot_path = os.path.abspath('all_files/baseline/plots')
            f_name = '_KPCA_Nearest_Points_' + name
            n_plot(frame, data_1, plot_path,f_name)
        elif each_file.endswith('p105.json') and each_file.startswith('kernel_pca'):
            print(each_file)
            name = each_file.split('.')[-2]
            data_1 = read_data(node2vec_path + '/' + each_file)
            frame = do_distance(each_file, data_2, node2vec_path)
            print(frame)
            plot_path = os.path.abspath('all_files/gdb/plots')
            f_name = '_KPCA_Nearest_Points_'+ name
            n_plot(frame, data_1, plot_path,f_name)
    # path = os.path.abspath('all_files/baseline/dimension_reduction')
    #
    # file = path + '/' + "kernel_pca_result_baseline.json"
    # file_2 = path_2 + '/' + "data_after_qc.json"
    # file_3 = path + '/' + "encoding.csv"
    # data_1 = read_data(file)
    # data_2 = read_data(file_2)
    # # data_3 = read_csv_files(file_3)
    #
    # # # print(data_1)
    # # data_2 = preprocess_data_for_distance_calculation(data_1)
    # # # print(preprocess_data_for_distance_calculation(data_1))
    # # distance_frame = distance(data_2)
    # # print(distance_frame)

    # plot(last_frame, data_1, plot_path, f_name)


if __name__ == '__main__':
    main()
