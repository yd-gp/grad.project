"""
    Copyright (C) 2021  Y.Dogay [dogayyagmur@gmail.com]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import os
from molecules_dframe import read_data
from merge_and_clean import create_folder
from dimension_reduction_plots import dimension_reduction_plot, t_sne_plot_with_tabs


def main():
    """
    calling functions to plot 2 dimension of graph data
    path to dimensionaly reducted data,
    creating a folder
    saving plots in a folder
    """
    folder_name_path = 'all_files/gdb/plots/'
    create_folder(folder_name_path)
    dimension_reducted_data = os.path.abspath('all_files/gdb/dimension_reduction')
    # data_json_path = os.path.abspath('all_files')
    list_of_files = os.listdir(dimension_reducted_data)
    path = os.path.abspath('all_files/gdb/plots')
    title_2 = 'Graph Data'

    for each_file in list_of_files:
        data = read_data(dimension_reducted_data + '/' + each_file)
        print(data)
        if each_file.startswith('mds_'):

            print(each_file)
            print(data)
            csv_name = each_file.split('.')[-2]
            title = "Multidimensional Scaling Analysis of"
            dimension_reduction_plot(data, title_2, title, path, csv_name)
        elif each_file.startswith('kernel'):
            csv_name = each_file.split('.')[-2]
            title = "Kernel Principle Component Analysis of"
            dimension_reduction_plot(data, title_2, title, path, csv_name)

        else:
            t_sne_datas = []
            t_sne_datas.append(each_file)
            csv_name = each_file.split('.')[-2]
            p_name = csv_name.split('_')[-1]
            print(csv_name)
            print(p_name)
            path_2= os.path.abspath('all_files/gdb/dimension_reduction')
            t_sne_plot_with_tabs(t_sne_datas, path=path_2, plot_title_2=title_2, plot_path=path)


if __name__ == '__main__':
    main()
