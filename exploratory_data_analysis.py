"""
    Copyright (C) 2021  Y.Dogay [dogayyagmur@gmail.com]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import os
from collections import Counter, defaultdict
from IPython.display import display
import pandas as pd
from molecules_dframe import read_data


def unique_values(data):
    """
    get the unique values for each columns
    """
    columns = ['category',
               'category_readable', 'alias', 'natural_source_name',
               ]
    for column in columns:
        print(column, set(data[column]), len(set(data[column])))


def category_frames(data):
    """
    get the data for each category
    """
    for i in data.category_readable.unique():
        print('------')
        print(data[data['category_readable'] == i])


def choose_only_plat_based_ingredients(data):
    """
    extracting information only based on plant based ingredients and putting them into a frame
    """
    plant_df = pd.DataFrame(columns=['pubchem_id', 'common_name', 'flavor_profile',
                                     'category', 'entity_id',
                                     'category_readable', 'alias', 'natural_source_name',
                                     'synonyms'])
    plants = ['Gourd', 'Vegetable', 'Fruit', 'Legume', 'Nut', 'Spice',
              'Berry', 'Herb', 'Plant Derivative', 'Plant',
              'Cabbage', 'Fungus', 'Flower', 'Vegetable Fruit'
        , 'Vegetable Tuber', 'Vegetable Root', 'Fruit-Berry',
              'Fruit Citrus', 'Seed', 'Vegetable Stem']

    for plant in plants:
        df2 = data[data['category_readable'] == plant]
        plant_df = plant_df.append([df2])
    plant_df.reset_index(drop=True, inplace=True)
    return plant_df


def most_common(plant_df):
    """
    EDA on plant based ingredients
    """
    counter = Counter(plant_df['common_name'][0])
    for i in plant_df['common_name'][1:]:
        counter.update(i)

    most = counter.most_common()

    n_dict = defaultdict(list)

    for item_1, item_2 in most:
        # n_dict[item_2]
        n_dict[item_2].append(item_1)
    return n_dict


def frame(most):
    """
    put the output into frame to see better
    """
    frequency_df = pd.DataFrame(list(most.items()), columns=['number_of_occurance', 'common_name'])
    return frequency_df


def eda_molecules(data):
    """
    eda on molecules
    """
    print(data.groupby('flavor_profile').groups)
    grouped = data.groupby('flavor_profile')
    for name, group in grouped:
        print(name, group)
    print(grouped.count())
    grouped = data.groupby('flavor_profile')
    for name, group in grouped:
        display(name, group)


def main():
    """
    calling functions to provide eda on data
    """
    path = os.path.abspath('all_files')
    file = path + '/' + "result_FDB.json"
    data = read_data(file)
    print(data.info)
    print(unique_values(data))
    category_frames(data)
    print(choose_only_plat_based_ingredients(data))
    print(choose_only_plat_based_ingredients(data).columns)
    plant_df = choose_only_plat_based_ingredients(data)
    most = most_common(plant_df)
    print(most)
    d_frame = frame(most)
    print(d_frame)
    path = os.path.abspath('all_files')
    file = path + '/' + "molecules.json"
    data = read_data(file)
    eda_molecules(data)


if __name__ == '__main__':
    main()
