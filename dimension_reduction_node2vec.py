"""
    Copyright (C) 2021  Y.Dogay [dogayyagmur@gmail.com]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import os
import pandas as pd
from merge_csvs import read_csv_files
from molecules_dframe import read_data
from merge_and_clean import create_folder
from dimension_reduction_baseline import make_three_main_categories,\
    change_category_to_plant_base, mds, t_sne_reduction, kernel_pca_reduction,\
    category_colors


def data_preparing(data, data_2, drop_columns):
    """
    removing [] from rows
    spliting row items with comma (,)
    renaming column
    putting row list values into separate columns
    """
    data = data[['name', 'label', 'embedding', 'nodeId']]

    columns = ['embedding', 'label']
    for column in columns:
        data.loc[:, column] = data.loc[:, column].str.replace(r"\[", "")
        data.loc[:, column] = data.loc[:, column].str.replace(r"\]", "")
    for i in range(len(data.embedding)):
        data.embedding[i] = data.embedding[i].split(',')

    data.rename(columns={'name': 'alias'}, inplace=True)

    data = pd.merge(data, data_2, on='alias')
    cols = []
    for i in range(1, 129):
        cols.append('d' + str(i))
    data[cols] = pd.DataFrame(data.embedding.tolist(), index=data.index)
    data[cols] = data[cols].astype(
        float)
    data.drop(drop_columns, axis=1, inplace=True)
    return data


def main():
    """
    calling functions to reduce dimensionalty of graph data
    path to graph data
    creating folder
    """
    merged_embedding_data = os.path.abspath('all_files/gdb/merged_folder')
    data_json_path = os.path.abspath('all_files')
    list_of_files = os.listdir(merged_embedding_data)
    # path = os.path.abspath('all_files/gdb')
    folder_name = 'all_files/gdb/dimension_reduction'
    create_folder(folder_name)
    for each_file in list_of_files:
        if each_file.startswith('gdb_'):
            print(each_file)
            csv_name = each_file.split('.')[-2]
            print(csv_name)
            data_1 = read_csv_files(merged_embedding_data + '/' + each_file)
            full_data = read_data(data_json_path + '/' + 'data_after_qc.json')
            data_columns = ['label', 'nodeId', 'category', 'entity_id', 'embedding',
                            'natural_source_name', 'synonyms', 'pubchem_id',
                            'common_name', 'flavor_profile']
            frame_1 = data_preparing(data_1, full_data, data_columns)
            print(frame_1)
            changed_category_frame = change_category_to_plant_base(frame_1)
            print(changed_category_frame)
            main_categories_frame = make_three_main_categories(changed_category_frame)
            print(main_categories_frame)
            colors_frame = category_colors(main_categories_frame, column='category_readable')

            data_5 = colors_frame.iloc[:, 2:-1]
            result_folder_path = os.path.abspath('all_files/gdb/dimension_reduction')
            name = 'mds_result_' + str(csv_name) + '.json'
            mds(colors_frame, data_5, result_folder_path, name)
            name = 'kernel_pca_result_' + str(csv_name) + '.json'
            kernel_pca_reduction(colors_frame, data_5, result_folder_path, name)
            name = 'tsne_result_' + str(csv_name) + '_perplexity_'
            t_sne_reduction(colors_frame, data_5, result_folder_path, name)


if __name__ == '__main__':
    main()
