"""
    Copyright (C) 2021  Y.Dogay [dogayyagmur@gmail.com]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import os
import warnings
from pearson_correlation import merge_frames, change_category_to_plant_base, \
    make_three_main_categories, reindex, create_data_frame, sorted_df, \
    s_compounds, percentage, save_frame
from merge_csvs import read_csv_files
from molecules_dframe import read_data
from merge_and_clean import create_folder
from eucledian_distace import create_lists_for_new_frame, select_rows
from dimension_reduction_node2vec import data_preparing

warnings.simplefilter(action='ignore', category=FutureWarning)


def baseline(each_file, baseline_path, full_data, function, name):
    """
    calling functions to calculate eucledian distance
    for baseline data
    """
    data = read_csv_files(baseline_path + '/' + str(each_file))
    merged_frame = merge_frames(data, full_data)
    print(merged_frame)
    print(merged_frame.columns)
    changed_category_frame = change_category_to_plant_base(merged_frame)
    print(changed_category_frame)
    main_category_frame = make_three_main_categories(changed_category_frame)
    print(main_category_frame)
    re_index_frame = reindex(main_category_frame)
    print(re_index_frame)
    print(re_index_frame.columns)
    lists = function(re_index_frame)
    columns = ['Ingredient_1', 'Ingredient_2', 'Distance',
               'Category', 'I_1_Pubchem_id',
               'I_2_Pubchem_id', 'I_1_Common_name', 'I_2_Common_name']
    new_frame = create_data_frame(lists, columns)
    print(new_frame)
    p_column = 'Distance'
    boolean = True
    sorted_frame = sorted_df(new_frame, p_column, boolean)
    selected_frame = select_rows(sorted_frame, number=50)
    c_frame = s_compounds(selected_frame)
    print(c_frame)
    last_frame = percentage(c_frame)
    path_3 = os.path.abspath('all_files/distance/')
    path_and_name = os.path.abspath(path_3 + '/' + str(name) + '.csv')
    save_frame(last_frame, path_and_name)
    print(last_frame)


def node2vec(each_file, node2vec_path, full_data, function, name):
    """
    calling functions to calculate eucledian distance
    for graph data
    """
    print(each_file)
    csv_name = each_file.split('.')[-2]
    data = read_csv_files(node2vec_path + '/' + str(each_file))
    print(data)
    columns = ['label', 'nodeId', 'category', 'entity_id', 'embedding',
               'natural_source_name', 'synonyms',
               'flavor_profile']
    data_2 = data_preparing(data, full_data, columns)
    print(data_2)
    print(data_2.columns)
    changed_category_frame = change_category_to_plant_base(data_2)
    print(changed_category_frame)
    main_category_frame = make_three_main_categories(changed_category_frame)
    print(main_category_frame)
    re_index_frame = reindex(main_category_frame)
    print(re_index_frame)
    print(re_index_frame.columns)
    lists = function(re_index_frame)
    columns = ['Ingredient_1', 'Ingredient_2', 'Distance',
               'Category', 'I_1_Pubchem_id',
               'I_2_Pubchem_id', 'I_1_Common_name', 'I_2_Common_name']
    new_frame = create_data_frame(lists, columns)
    print(new_frame)
    p_column = 'Distance'
    boolean = True
    sorted_frame = sorted_df(new_frame, p_column, boolean)
    selected_frame = select_rows(sorted_frame, number=50)
    c_frame = s_compounds(selected_frame)
    print(c_frame)
    last_frame = percentage(c_frame)
    path_3 = os.path.abspath('all_files/distance/')
    path_and_name = os.path.abspath(path_3 + '/' + str(name) + str(csv_name) + '.csv')
    save_frame(last_frame, path_and_name)
    print(last_frame)


def main():
    """
    calling functions to run the scripts
    """
    path = os.path.abspath('all_files')
    folder_name_path = 'all_files/distance'
    create_folder(folder_name_path)
    json_file = path + '/' + 'data_after_qc.json'
    full_data = read_data(json_file)
    baseline_path = os.path.abspath('all_files/baseline')
    node2vec_path = os.path.abspath('all_files/gdb/merged_folder')
    baseline_files = os.listdir(baseline_path)
    node2vec_files = os.listdir(node2vec_path)
    both_files = baseline_files + node2vec_files
    for each_file in both_files:
        if each_file.endswith('.csv') and each_file.startswith('enco'):
            print(each_file)
            name = 'e_distance_baseline'
            baseline(each_file, baseline_path, full_data, create_lists_for_new_frame, name)
        elif each_file.endswith('.csv') and each_file.startswith('gdb'):
            name = 'e_distance_'
            node2vec(each_file, node2vec_path, full_data, create_lists_for_new_frame, name)


if __name__ == '__main__':
    main()
