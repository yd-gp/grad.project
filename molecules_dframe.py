"""
    Copyright (C) 2021  Y.Dogay [dogayyagmur@gmail.com]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import os
import re
import pandas as pd


def read_data(file):
    """
    read json file
    """
    data = pd.read_json(file)
    return data


def mol(data):
    """
    read data, get wanted columns and explode each row list to another row
    drop duplicates, reset index
    """
    molecules = data[['pubchem_id', 'common_name', 'flavor_profile']]
    molecules = molecules.copy()
    molecules['index'] = molecules.index
    molecules = molecules.set_index(['index']).apply(pd.Series.explode).reset_index()
    molecules.drop(['index'], axis=1, inplace=True)
    molecules = molecules.drop_duplicates()
    molecules.reset_index(drop=True, inplace=True)
    return molecules


def replace_at_with_comma(str1):
    """
    replace @ with ,
    """
    return re.sub(r"@", r", ", str1)


def set_changed_values(data):
    """
    use replace_at_with_comma(str1) function to replace @ with , for flavor_profile column values
    """
    for i in data['flavor_profile']:
        new1 = replace_at_with_comma(i)
        data['flavor_profile'] = data['flavor_profile'].replace([i], new1)
    return data


def save_data_frame(data, path):
    """
    save data frame as json file
    """
    data.to_json(path + 'molecules.json')


def main():
    """
    path to result_FDB json file, calling functions to run the script
    """
    path = os.path.abspath('all_files')
    file = path + '/' + "result_FDB.json"
    data_2 = read_data(file)
    data = mol(data_2)
    set_changed_values(data)
    path = 'all_files/'
    save_data_frame(data, path)
    print(set_changed_values(data))


if __name__ == '__main__':
    main()
