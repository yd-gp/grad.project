"""
    Copyright (C) 2021  Y.Dogay [dogayyagmur@gmail.com]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import os
import pandas as pd
from merge_csvs import read_csv_files
from pearson_correlation import save_frame


def clean_set(data):
    """
    if there is a column named 'Unnamed: 0' drop it
    """
    for column in data.columns:
        if column =='Unnamed: 0':
            data.drop([column],axis=1, inplace=True)
        else:
            continue
    return data


def choose(data_3):
    """
    create a new frame only including plant based categories
    """
    data_4 = data_3[data_3['Category'] == 'Plant_Based']
    data_4.reset_index(drop=True, inplace=True)
    data = pd.DataFrame(columns=data_4.columns)
    data5 = data_4.copy()
    meats = ['beef', 'chicken', 'pork']
    for meat in meats:
        frame = data5[data5['Ingredient_1'] == meat]
        data = data.append(frame, ignore_index=True)
    return data


def main():
    """
    calling functions to run the script,
    to get a frame of only plant based ingredients correlation values
    and saving it as csv
    """
    path = os.path.abspath('all_files/baseline/correlation')
    correlation_file_name = 'pearson_correlation.csv'
    full_data = read_csv_files(path + '/' + correlation_file_name)
    print(full_data)
    data_2 = clean_set(full_data)
    print(data_2)
    plant_data = choose(data_2)
    print(plant_data)
    path_and_name = os.path.abspath(path + '/' + 'plant_based_pearson_correlation' + '.csv')
    save_frame(plant_data, path_and_name)


if __name__ == '__main__':
    main()
