"""
    Copyright (C) 2021  Y.Dogay [dogayyagmur@gmail.com]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import socket
import os
import paramiko


def get_host_name_ip():
    """
    Return Hostname and Host Ip
    """
    try:
        host_name = socket.gethostname()
        host_ip = socket.gethostbyname(host_name)
        print("Hostname :  ", host_name)
        print("IP : ", host_ip)
        return host_ip
    except LookupError:
        print("Unable to get Hostname and IP")


def server_connection(localhost_ip, username, password):
    """
    connect to server
    """
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(localhost_ip, username=username, password=password)
    # files=['eucledian_distance.py', 'distance_for_all.py','distance_plot.py']
    return ssh


def dimension_reduction_files(ssh):
    """
    run scripts on client
    """
    path_3 = os.path.abspath('dimension_reduction_node2vec.py')
    path_4 = os.path.abspath('dimension_reduction_node2vec_plots.py')

    ssh.exec_command(str(os.system("python3" + " " + path_3)) + '\n' + str(os.system("python3" + " "
                                                                                     + path_4)))
    ssh.close()


def main():
    """
    set input function for username and password
    run different scripts
    run functions
    """
    local_host = get_host_name_ip()
    username = input('Write your username')
    password = input('Write your password')
    # server(local_host, username, password)
    connection = server_connection(local_host, username, password)
    dimension_reduction_files(connection)


if __name__ == '__main__':
    main()
