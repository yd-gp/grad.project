"""
    Copyright (C) 2021  Y.Dogay [dogayyagmur@gmail.com]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import os
import re
import pandas as pd
from molecules_dframe import read_data


def molecules(frame):
    """
    read file, get wanted columns, set index, explode list of rows into seperate rows
    """
    molecules_frame = frame[['pubchem_id', 'common_name', 'flavor_profile']]
    molecules_df = molecules_frame.copy()
    molecules_df['index'] = molecules_df.index
    molecules_df = molecules_df.set_index(['index']).apply(pd.Series.explode).reset_index()
    molecules_df.drop(['index'], axis=1, inplace=True)
    molecules_df = molecules_df.drop_duplicates()
    molecules_df.reset_index(drop=True, inplace=True)
    return molecules_df


def replace_at_with_comma(string):
    """
    replacing '@' with ','
    """
    return re.sub(r"@", r", ", string)


def save(data, path):
    """
    save dataframe as json
    """
    data.to_json(path + '/' + 'molecules_after_qc.json', orient='records')


def main():
    """
    path to data_after_qc json file, reading file
    calling functions to run this script
    """
    path = os.path.abspath('all_files')
    file = path + '/' + 'data_after_qc.json'
    data = read_data(file)
    data_file = molecules(data)
    print(data_file)
    for i in data_file['flavor_profile']:
        new1 = replace_at_with_comma(i)
        data_file['flavor_profile'] = data_file['flavor_profile'].replace([i], new1)
    print(data_file['flavor_profile'])
    data_file['label'] = 'COMPOUND_NAME'
    print(data_file.columns)
    print(data_file)
    print(data_file[data_file['pubchem_id'] == 6202])
    print(len(data_file.pubchem_id.unique()))
    print(len(data_file.common_name.unique()))
    print(data_file[data_file.duplicated(['common_name'])])
    print(data_file[data_file['common_name'] == 'Phytol'])
    print(data_file[data_file['common_name'] == 'Sabinene hydrate'])
    save(data_file, path)


if __name__ == '__main__':
    main()
