"""
    Copyright (C) 2021  Y.Dogay [dogayyagmur@gmail.com]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
from bokeh.models import ColumnDataSource, HoverTool, Panel, Tabs, Legend, Title
from bokeh.plotting import figure, output_file, show
from molecules_dframe import read_data
from merge_and_clean import create_folder


def dimension_reduction_plot(data, plot_title_1, plot_title_2, plot_path, name):
    """
    plotting 2 dimension baseline data,
    mouse over dots showing dimension 1 and 2 value, category and
    ingredient name
    saving plot in a folder
    """
    source = ColumnDataSource(data=dict(x=data.dimension1, y=data.dimension2,
                                        desc=data.alias,
                                        category=data.category_readable, coluor=data.color))
    hover = HoverTool(tooltips=[
        ("category", "@category"),
        ("(x,y)", "(@x, @y)"),
        ('name', '@desc'),
    ])
    plot = figure(plot_width=400, plot_height=400, tools=[hover, 'pan', 'wheel_zoom',
                                                       'box_zoom', 'zoom_out', 'save'],
               x_axis_label="Dimension 1",
               y_axis_label="Dimension 2", background_fill_color="grey", background_fill_alpha=0.1)

    plot.axis.axis_label_text_font_style = 'normal'
    plot.add_layout(Legend(), 'below')
    plot.circle('x', 'y', size=5, source=source, alpha=0.5, fill_alpha=0.2, color='coluor',
             legend_group='category')
    plot.add_layout(Title(text=str(plot_title_1), text_font_size="9pt", align="center",
                       text_line_height=0.5),
                 'above')
    plot.add_layout(Title(text=str(plot_title_2), text_font_size="9pt", align="center",
                       text_line_height=0.5), 'above')
    plot.legend.glyph_height = 7
    plot.legend.title = 'Ingredient Categories'
    plot.legend.title_text_font_style = "bold"
    plot.legend.title_text_font_size = "9px"
    # increasing the glyph width
    plot.legend.glyph_width = 20

    # Increasing the glyph's label height
    plot.legend.label_height = 10

    # Increasing the glyph's label height
    plot.legend.label_width = 10

    # change appearance of legend text
    plot.legend.label_text_font = "times"
    plot.legend.label_text_font_style = "italic"
    plot.legend.label_text_color = "black"
    plot.legend.label_text_font_size = "7pt"
    plot.legend.orientation = 'horizontal'
    # change border and background of legend
    plot.legend.border_line_width = 0.1
    plot.legend.border_line_color = "black"
    plot.legend.border_line_alpha = 0.8
    plot.legend.background_fill_color = "white"
    plot.legend.background_fill_alpha = 0.1
    output_file(str(plot_path) + '/' + str(name) + '.html')
    show(plot)


def t_sne_plot_with_tabs(datas, path, plot_title_2, plot_path):
    """
    plotting 2 dimension baseline data,
    mouse over dots showing dimension 1 and 2 value, category and
    ingredient name
    for each different perplexity putting them as tabs
    saving plot in a folder
    """
    tabs = []
    for data in datas:
        json_name = data.split('.')[-2]
        p_name = json_name.split('_')[-1]

        data = read_data(path + '/' + data)
        source = ColumnDataSource(data=dict(x=data.dimension1, y=data.dimension2,
                                            desc=data.alias,
                                            category=data.category_readable, coluor=data.color))
        hover = HoverTool(tooltips=[
            ("category", "@category"),
            ("(x,y)", "(@x, @y)"),
            ('name', '@desc'),
        ])
        plot = figure(plot_width=400, plot_height=400, tools=[hover, 'pan',
                                                              'wheel_zoom', 'box_zoom',
                                                              'zoom_out', 'save'],
                   x_axis_label="Dimension 1",
                   y_axis_label="Dimension 2", background_fill_color="grey",
                   background_fill_alpha=0.1)
        plot.axis.axis_label_text_font_style = 'normal'
        plot.add_layout(Legend(), 'below')
        plot.circle('x', 'y', size=5, source=source, alpha=0.5, fill_alpha=0.2,
                 color='coluor', legend_group='category')
        # p.legend.location = "bottom_right"
        plot.add_layout(
            Title(text="Analysis of " + str(plot_title_2), text_font_size="9pt",
                  align="center", text_line_height=0.5),
            'above')
        plot.add_layout(Title(text="t-distributed Stochastic Neighbor Embedding",
                           text_font_size="9pt", align="center",
                           text_line_height=0.5), 'above')

        plot.title.align = "center"
        plot.title.text_font_size = '8pt'
        plot.legend.glyph_height = 7
        plot.legend.title = 'Ingredient Categories'
        plot.legend.title_text_font_style = "bold"
        plot.legend.title_text_font_size = "9px"
        # increasing the glyph width
        plot.legend.glyph_width = 20

        # Increasing the glyph's label height
        plot.legend.label_height = 10

        # Increasing the glyph's label height
        plot.legend.label_width = 10

        # change appearance of legend text
        plot.legend.label_text_font = "times"
        plot.legend.label_text_font_style = "italic"
        plot.legend.label_text_color = "black"
        plot.legend.label_text_font_size = "7pt"
        plot.legend.orientation = 'horizontal'
        # change border and background of legend
        plot.legend.border_line_width = 0.1
        plot.legend.border_line_color = "black"
        plot.legend.border_line_alpha = 0.8
        plot.legend.background_fill_color = "white"
        plot.legend.background_fill_alpha = 0.1

        tab = Panel(child=plot, title='Perplexity: ' + str(p_name))
        tabs.append(tab)

        # tabs_object = Tabs(tabs=plots)
        # path = os.path.abspath('all_files/plots')
        # output_file(str(path) + '/' + str(name) + '.html')
        # save(tabs_object)
        # show(tabs_object)
        # print(tsne_results)
    tabs_object = Tabs(tabs=tabs)

    output_file(str(plot_path) + '/' + str(json_name) + '.html')
    show(tabs_object)


def main():
    """
    plottind two dimension of baseline data with some bokeh interactive tools
    path to dimensionaly reducted data,
    creating a folder
    """
    folder_name_path = 'all_files/baseline/plots/'
    create_folder(folder_name_path)
    dimension_reducted_data = os.path.abspath('all_files/baseline/dimension_reduction')
    # data_json_path = os.path.abspath('all_files')
    list_of_files = os.listdir(dimension_reducted_data)
    path = os.path.abspath('all_files/baseline/plots')
    title_2 = 'Baseline Data'
    tsne_datas = []
    for each_file in list_of_files:
        data = read_data(dimension_reducted_data + '/' + each_file)
        if each_file.startswith('mds_'):

            print(each_file)
            print(data)
            csv_name = each_file.split('.')[-2]
            title = "Multidimensional Scaling Analysis of"
            dimension_reduction_plot(data, title_2, title, path, csv_name)
        elif each_file.startswith('kernel'):
            csv_name = each_file.split('.')[-2]
            title = "Kernel Principle Component Analysis of"
            dimension_reduction_plot(data, title_2, title, path, csv_name)

        else:
            tsne_datas.append(each_file)
            # csv_name = each_file.split('.')[-2]
            # p_name = csv_name.split('_')[-1]
            # print(csv_name)
            # print(p_name)

    path_2 = os.path.abspath('all_files/baseline/dimension_reduction')
    t_sne_plot_with_tabs(tsne_datas, path=path_2, plot_title_2=title_2, plot_path=path)


if __name__ == '__main__':
    main()
