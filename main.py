"""
    Copyright (C) 2021  Y.Dogay [dogayyagmur@gmail.com]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import threading
import os
import paramiko
from baseline_dimension_reduction import get_host_name_ip, server_connection


def obtaining_data(ssh):
    """
    run scripts on client
    """
    path_1 = os.path.abspath('data_collection.py')
    ssh.exec_command(
        str(os.system("python3" + " " + path_1)))


def preprocess_files(ssh):
    """
    run scripts on client
    """
    path_1 = os.path.abspath('merge_and_clean.py')
    path_2 = os.path.abspath('molecules_dframe.py')
    path_3 = os.path.abspath('exploratory_data_analysis.py')
    path_4 = os.path.abspath('ingredients_quality_check.py')
    path_5 = os.path.abspath('molecules_dataframe_after_qc.py')
    ssh.exec_command(
        str(os.system("python3" + " " + path_1))
        + '\n' + str(os.system("python3" + " "
                               + path_2)) + '\n' +
        str(os.system("python3" + " " + path_3)) + '\n' + str(os.system("python3" + " "
                                                                        + path_4)) + '\n' + str(
            os.system("python3" + " "
                      + path_5)))


def baseline_data(ssh):
    """
    run scripts on client
    """
    path_1 = os.path.abspath('b_data.py')
    path_2 = os.path.abspath('pearson_correlation.py')
    path_3 = os.path.abspath('correlation_plant_based.py')
    ssh.exec_command(
        str(os.system("python3" + " " + path_1))
        + '\n' + str(os.system("python3" + " "
                               + path_2)) + str(os.system("python3" + " " + path_3)))


def graph_data(ssh):
    """
    run scripts on client
    """
    path_1 = os.path.abspath('graph_database.py')
    path_2 = os.path.abspath('node2vec_algorithm.py')
    path_3 = os.path.abspath('merge_csvs.py')
    ssh.exec_command(
        str(os.system("python3" + " " + path_1))
        + '\n' + str(os.system("python3" + " "
                               + path_2)) + str(os.system("python3" + " " + path_3)))


def distance_files(ssh):
    """
    run scripts on client
    """
    path_1 = os.path.abspath('distance_for_all.py')
    path_2 = os.path.abspath('cosine_distance.py')
    path_3 = os.path.abspath('distance_plot.py')
    ssh.exec_command(
        str(os.system("python3" + " " + path_1))
        + '\n' + str(os.system("python3" + " "
                               + path_2)) + '\n' + str(os.system("python3" + " " + path_3)))


def clustering_and_n_neigbors(ssh):
    """
    run scripts on client
    """
    path_1 = os.path.abspath('nearest_neigbours.py')
    path_2 = os.path.abspath('clustering_k_means.py')
    ssh.exec_command(
        str(os.system("python3" + " " + path_1)) + '\n' + str(os.system("python3" + " "
                                                                        + path_2)))


def compounds_and_flavors(ssh):
    """
    run scripts on client
    """
    path_1 = os.path.abspath('shared_compounds.py')
    path_2 = os.path.abspath('shared_flavors.py')
    ssh.exec_command(
        str(os.system("python3" + " " + path_1)) + '\n' + str(os.system("python3" + " "
                                                                        + path_2)))
    ssh.close()


def clients(host, cmd, username, password):
    """
    connect to client with ssh
    run scripts on client
    """
    out_lock = threading.Lock()
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(host, username=username, password=password)
    # ssh.exec_command(cmd)
    stdin, stdout, stderr = ssh.exec_command(cmd)
    print(stdin, stderr)

    with out_lock:
        print(stdout.readlines())

    ssh.close()


def main():
    """
    set input function for username and password
    for loop to:
        manage thread for clients
        and run different scripts
    run functions
    """
    local_host = get_host_name_ip()
    username = input('Write your username')
    password = input('Write your password')
    hosts = [local_host] * 2
    # server(local_host, username, password)
    connection = server_connection(local_host, username, password)
    obtaining_data(connection)
    preprocess_files(connection)
    baseline_data(connection)
    graph_data(connection)
    distance_files(connection)
    path_1 = os.path.abspath("baseline_dimension_reduction.py")
    path_2 = os.path.abspath('graph_dimension_reduction.py')
    cmd = [str(os.system("python3" + " " + path_1))
           + '\n' + str(os.system("python3"
                                  + " " + path_2))]
    threads = []
    for host, command in zip(hosts, cmd):
        thread = threading.Thread(target=clients, args=(host, command, username, password))
        thread.start()
        threads.append(thread)
    for thread in threads:
        thread.join()
    clustering_and_n_neigbors(connection)
    compounds_and_flavors(connection)


if __name__ == '__main__':
    main()
