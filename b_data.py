"""
    Copyright (C) 2021  Y.Dogay [dogayyagmur@gmail.com]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import os
import pandas as pd
from molecules_dframe import read_data
from merge_and_clean import create_folder


def baseline(data_1, data_2, path):
    """
    explode row values into columns,
    reset index,
    add ingredient names into new column as alias
    """
    data = data_1[['alias', 'common_name']]
    data = data.explode('common_name')
    data_3 = pd.get_dummies(data.common_name)
    data_3['index'] = data.index
    # merge rows
    data_3 = data_3.groupby(['index']).agg('sum')
    data_3['alias'] = data_2.alias
    data_3.set_index('alias', inplace=True)
    data_3.apply(pd.to_numeric)
    for column in data_3.columns:
        data_3[column] = data_3[column].apply(lambda x: 1 if x >= 2 else x)
    data_3.to_csv(path + '/' + 'encoding.csv')
    return data_3


def main():
    """
    reading file,
    getting path
    calling functions
    """
    path = os.path.abspath('all_files')
    file_name = 'data_after_qc.json'
    data = read_data(path + '/' + file_name)
    print(data)
    folder_name = 'all_files/baseline'
    create_folder(folder_name)
    path_2 = 'all_files/baseline/'
    data_2 = baseline(data, data, path_2)
    print(data_2)


if __name__ == '__main__':
    main()
