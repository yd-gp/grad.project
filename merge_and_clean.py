"""
    Copyright (C) 2021  Y.Dogay [dogayyagmur@gmail.com]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import multiprocessing
import os
from collections import defaultdict
from datetime import datetime
from functools import partial
from multiprocessing import Pool
import numpy as np
import pandas as pd


def read_json_files(file):
    """
    reading json file
    """
    path_to_json = os.path.abspath('FlavorDB_Json_Files')
    data = pd.read_json(path_to_json + '/' + file, lines=True)
    return data


def load_files_as_data_frame(path):
    """
    loading all json files as dataframe from folder
    """
    path_to_json = os.path.abspath(path)
    data_2 = pd.DataFrame()
    for file_name in [file for file in os.listdir(path_to_json) if file.endswith('.json')]:
        data = pd.read_json(path_to_json + '/' + file_name, lines=True)
        data_2 = data_2.append(data)
    return data_2


def drop_columns_reset_index(data):
    """
    drop unwanted columns and reset index
    """
    data.drop(['entity_alias', 'entity_alias_basket', 'natural_source_url',
               'entity_alias_url'], axis=1, inplace=True)
    data = data.reset_index(drop=True)
    return data


def sub_molecule(data):
    """
    extracting pubchem id, flavor profile and common name sub columns from molecules column
    creating list of dictionaries for pubchem_id, common name and flavor profile
    assigning their key as pubchem_id/common name/ flavor profile + their index number
    assigning their values as the pubchem id that that contain
    """
    pubchem_id = []
    common_name = []
    flavor_profile = []
    for i in range(len(data['molecules'])):
        for item in data['molecules'][i]:
            for key, value in item.items():
                if key == 'pubchem_id':
                    pubchem_id.append({key + '_' + str(i): value})
                elif key == 'common_name':
                    common_name.append({key + '_' + str(i): value})
                elif key == 'flavor_profile':
                    flavor_profile.append({key + '_' + str(i): value})
    # print(pubchem_id)
    # print(common_name)
    # print(flavor_profile)
    return pubchem_id, common_name, flavor_profile


def create_column(data):
    """
    creating new columns with names pubchem id, common name and flavor profile.
    Assigning their rows as nan values
    """
    data['pubchem_id'] = np.nan
    data['common_name'] = np.nan
    data['flavor_profile'] = np.nan
    return data


def create_dicts_from_sub_molecules(lists, df_molecule_column):
    """
    write the same keys values under a single key
    e.g: 0:[[6202, 644104, 8094]] here key is index and values are pubchem ids
    """
    p_id = defaultdict(list)
    c_name = defaultdict(list)
    f_profile = defaultdict(list)
    for i in range(len(df_molecule_column)):
        len(p_id[i])
        len(c_name[i])
        len(f_profile[i])
        for item in lists:
            for key, value in item.items():
                if key == 'pubchem_id_' + str(i):
                    p_id[i].append(value)
                elif key == 'common_name_' + str(i):
                    c_name[i].append(value)
                elif key == 'flavor_profile_' + str(i):
                    f_profile[i].append(value)
    return p_id, c_name, f_profile


def lists_rows(p_id, c_name, f_profile):
    """
    creating an empty list and putting all values from create_dicts_from_sub_molecules
    function into nested lists
    """
    pubchem_id_row = []
    common_name_row = []
    flavor_profile_row = []
    for value in p_id.values():
        pubchem_id_row.append(value)
    for value in c_name.values():
        common_name_row.append(value)
    for value in f_profile.values():
        flavor_profile_row.append(value)
    # print(pubchem_id_row)
    # print(key,value)
    return pubchem_id_row, common_name_row, flavor_profile_row


def sub_molecules_dictionary(pubchem_id_row, common_name_row, flavor_profile_row):
    """
    creating dictionary for pubchem id, common name and flavor profile
    """
    dictionary = {'pubchem_id': [], 'common_name': [], 'flavor_profile': []}
    dictionary['pubchem_id'] = pubchem_id_row
    dictionary['common_name'] = common_name_row
    dictionary['flavor_profile'] = flavor_profile_row
    return dictionary


def dictionary_to_frame(dictionary):
    """
    creating dataframe from dictionary
    """
    data = pd.DataFrame(dictionary)
    return data


def drop_molecules(data):
    """
    dropping molecules column
    """
    data.drop(['molecules'], axis=1, inplace=True)
    data = data.reset_index(drop=True)
    return data


def create_folder(folder_name):
    """
    create folder
    """
    if os.path.exists('./' + folder_name):
        pass
    else:
        os.mkdir('./' + folder_name)


def merge_frames(data1, data2, path):
    """
    concating two dataframes
    saving it as json file
    """
    result = pd.concat([data1, data2], axis=1, join='inner')
    # display(result)
    result.rename(columns={'entity_alias_readable': 'alias',
                           'entity_alias_synonyms': 'synonyms'}, inplace=True)
    result.to_json(path + '/' + 'result_FDB.json')
    return result


def speed():
    """
       multiprocessing to increase the speed of run time
       calling functions; drop_columns_reset_index, sub_molecule, read_json_files
    """
    start_time = datetime.now()
    path_to_json = os.path.abspath('FlavorDB_Json_Files')
    file_lists = [file for file in os.listdir(path_to_json) if file.endswith('.json')]
    path = 'all_files'
    create_folder(path)

    with Pool(processes=multiprocessing.cpu_count()) as pool:
        # have your pool map the file names to dataframes
        df_list = pool.map(read_json_files, file_lists)
        # reduce the list of dataframes to a single dataframe
        combined_df = pd.concat(df_list, ignore_index=True)
    dataa = drop_columns_reset_index(combined_df)
    sub_molecules = sub_molecule(dataa)
    end_time = datetime.now()
    print('Duration: {}'.format(end_time - start_time))
    return sub_molecules, combined_df, dataa


def main():
    """
       calling functions to run the file
       multiprocessing to increase the speed of run time
    """

    sub_molecules = speed()
    pubchem_id = sub_molecules[0][0]
    common_name = sub_molecules[0][1]
    flavor_profile = sub_molecules[0][2]
    start_time = datetime.now()
    lists = [pubchem_id, common_name, flavor_profile]
    with Pool(processes=multiprocessing.cpu_count()) as pool:
        # have your pool map the file names to dataframes
        df_list = pool.map(partial(create_dicts_from_sub_molecules,
                                   df_molecule_column=sub_molecules[2]['molecules']), lists)
    end_time = datetime.now()
    print('Duration: {}'.format(end_time - start_time))
    rows = lists_rows(df_list[0][0], df_list[1][1], df_list[2][2])
    dictionary = sub_molecules_dictionary(rows[0], rows[1], rows[2])
    data2 = dictionary_to_frame(dictionary)
    data1 = drop_molecules(sub_molecules[1])
    path = 'all_files'
    merge_frames(data1, data2, path)
    print(merge_frames(data1, data2, path))


if __name__ == '__main__':
    main()
