"""
    Copyright (C) 2021  Y.Dogay [dogayyagmur@gmail.com]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from graph_database import connect_neo4j
from merge_and_clean import create_folder


def export_graph_database_into_csv(session):
    """
    saving graph as csv
    """
    query_0 = '''CALL apoc.export.csv.query("MATCH (n) return n", "file:///'''
    print('Enter all_files folder path')
    query_1 = input()
    query_2 = '''/GDB.csv", {})'''
    query = query_0 + query_1 + query_2
    return session.run(query)


def save_graph_with_name(session):
    """
    saving graph with a name in order to later call and apply
    node2vec algorithm
    """
    query_1 = '''
    CALL gds.graph.create('Gr', ['INGREDIENT','COMPOUND_NAME'],{undirected:{type:'*', orientation:'UNDIRECTED'}})
    '''
    session.run(query_1)


def node2cev_128d_pq12(session):
    """
    calling graph with a given name and
    applying node2vec algorithm
    """
    query_0 = '''CALL apoc.export.csv.query("CALL gds.alpha.node2vec.stream('Gr',{inOutFactor:2.0, returnFactor:1.0, embeddingDimension: 128})", "file:///'''
    print('Enter all_files folder path')
    query_1 = input()
    query_2 = '''/embedding_128d_p12.csv", {})'''
    query = query_0 + query_1 + query_2
    session.run(query)


def node2cev_128d_pq105(session):
    """
    calling graph with a given name
    and applying node2vec algorithm
    """
    query_0 = '''CALL apoc.export.csv.query("CALL gds.alpha.node2vec.stream('Gr',{inOutFactor:0.5, returnFactor:1.0, embeddingDimension: 128})", "file:///'''
    print('Enter all_files folder path')
    query_1 = input()
    query_2 = '''/embedding_128d_p105.csv", {})'''
    query = query_0 + query_1 + query_2
    session.run(query)


def main():
    """
    Put your ip_adress, user and password
    Enter folder path
    Example: users/yagmur/Desktop/grad.project/all_files/gdb/
    """

    folder_name_path = 'all_files/gdb/'
    create_folder(folder_name_path)
    ip_adress = '127.0.0.1:7687'
    user = 'neo4j'
    password = '1234'
    connection = connect_neo4j(ip_adress, user, password)
    export_graph_database_into_csv(connection)
    save_graph_with_name(connection)
    node2cev_128d_pq12(connection)
    node2cev_128d_pq105(connection)


if __name__ == '__main__':
    main()
