# Identification of Plant-Based Ingredients as Potential Meat Substitutes #

There are potential ways of replacing meat-based ingredients (beef, chicken and pork) to more healthy alternatives as plant-based ingredients with using data of ingredients, their categories, chemical compounds and flavor profiles which related to their chemicals compounds. Specifically the ultimate research question is: can embedding, similarity, dimensionality reduction, and cluster techniques provide valid plant-based suggestions for meat based ingredients?

### What is this repository for? ###

* This is the repository for the Graduation Project.


### How do I get set up? ###

* First clone the master branch, secondly create a separate branch.


### Requirements ###
##### PyCharm (2019.2.2)
* bokeh (2.3.2)
* matplotlib (3.4.2)
* neo4j (4.2.1)
* numpy (1.20.2) 
* pandas (1.2.3) 
* scipy (1.6.3)
* sklearn (0.0) 
* sqlite (3.30.0)
##### Neo4j Desktop (1.4.4)
###### local graph DBMS (4.2.1)
* Awesome Procedures for Cypher (APOC) (4.2.0.2) 
* Graph Data Science Library (1.4.1)


To install all the required Python 3 packages at once:

pip3 install bokeh matplotlib neo4j numpy pandas scipy skearn sqlite

### How to run the application  ###
As shown in the table below, the files can be run according to the order

required input command example: python3 b_data.py

Note: Some files needs additional information to run. In those files it is written if it needed.
The information/s that need to pass to command line pop ups on the terminal as an input.

![Scheme](filerun_order.png)

#### or 
To run the whole program; the program needs to run from the commandline

required input command: python3 main.py

Note: For the sql data please contact the admin
### Contribution guidelines ###

* Please do not merge with master directly but create a side branch.
* Commit your changes to the side branch.
* When you want to add your branch to the project create a pull request.



### Author ###
Yagmur Dogay 

### Who do I talk to? ###

Any pressing questions reach out to admin [dogayyagmur@gmail.com]