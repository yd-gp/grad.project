"""
    Copyright (C) 2021  Y.Dogay [dogayyagmur@gmail.com]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from neo4j import GraphDatabase


def connect_neo4j(ip_adress, user, password):
    """
    connection to neo4j desktop server
    """
    driver = GraphDatabase.driver(uri='bolt://' + str(ip_adress), auth=(str(user), str(password)))
    session = driver.session()
    return session


def graph_database(session):
    """
    setting ingredients as nodes,
    setting compounds as nodes
    setting entity id, category as properties to ingredient labeled nodes
    creating relationship between ingredient label node and
    compound labeled node
    """
    line_0 = """call apoc.load.json("""
    print('Enter data_after_qc.json Path')
    line_1 = input()
    line_2 = """)
    yield value
    merge (a:INGREDIENT{NAME:value.alias, ENTITY_ID:value.entity_id, CATEGORY: value.category_readable})
    with value,a
    unwind value.common_name as common_name
    merge(b:COMPOUND_NAME{NAME:common_name})
    merge (b)-[:compound_part_of]->(a)
    """

    query = line_0 + line_1 + line_2
    return session.run(query)


def add_properties(session):
    """
    setting flavors and pubchem id of comounds as a
    property to compound labeled nodes
    """
    line_0 = """call apoc.load.json("""
    print('Enter molecules_after_qc.json Path')
    line_1 = input()
    line_2 = """)
    yield value
    unwind value.label as type
    unwind value.common_name as common_name
    unwind value.pubchem_id as pubchem_id
    unwind value.flavor_profile as flavor_profile
    match (b{NAME:common_name})
    unwind labels(b) as label
    with b,label,pubchem_id,type,flavor_profile
    where label=type
    set b.PUBCHEM_ID=pubchem_id
    set b.FLAVOR_PROFILE= flavor_profile
    return b"""

    query = line_0 + line_1 + line_2
    session.run(query)


def main():
    """
    Put your ip_adress, user and password
    Enter paths in Terminal
    Path Example: "file:///users/yagmur/Desktop/grad.project/all_files/data_after_qc.json"
    "file:///users/yagmur/Desktop/grad.project/all_files/molecules_after_qc.json"
    """
    ip_adress = '127.0.0.1:7687'
    user = 'neo4j'
    password = '1234'
    connection = connect_neo4j(ip_adress, user, password)
    # 127.0.0.1: 7687, neo4j, 1234
    # print('Enter ip, user, password')
    graph_database(connection)
    add_properties(connection)


if __name__ == '__main__':
    main()
