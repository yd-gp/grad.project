"""
    Copyright (C) 2021  Y.Dogay [dogayyagmur@gmail.com]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
from merge_csvs import read_csv_files
from pearson_correlation import create_folder, create_data_frame, s_compounds, shared_compounds_percentage, percentage, \
    sorted_df, save_frame
from molecules_dframe import read_data
import pandas as pd
from correlation_plant_based import clean_set


def merge(data, full_data):
    full_data = full_data[['alias', 'pubchem_id', 'flavor_profile', 'common_name']]
    data_3 = pd.merge(data, full_data, on='alias')
    data_3 = clean_set(data_3)
    return data_3


def molecules(data_3):
    n_data = data_3.copy()
    i_1 = []
    i_2 = []
    category = []
    p_id_1 = []
    p_id_2 = []
    c_name_1 = []
    c_name_2 = []
    for i in range(0, 3):
        for ii in range(len(n_data.alias)):
            print('------------')
            print(n_data['alias'][i], n_data['alias'][ii])
            catego = n_data['category_readable'][ii]
            i_1.append(n_data['alias'][i])
            i_2.append(n_data['alias'][ii])
            p_id_1.append(n_data['pubchem_id'][i])
            p_id_2.append(n_data['pubchem_id'][ii])
            c_name_1.append(n_data['common_name'][i])
            c_name_2.append(n_data['common_name'][ii])
            category.append(catego)

    df_list = [i_1, i_2, category, p_id_1, p_id_2, c_name_1, c_name_2]
    return df_list


def process(df_list):
    columns = ['Ingredient_1', 'Ingredient_2',
               'Category', 'I_1_Pubchem_id',
               'I_2_Pubchem_id', 'I_1_Common_name', 'I_2_Common_name']
    frame = create_data_frame(df_list, columns=columns)
    frame.reset_index(drop=True, inplace=True)
    s_frame = s_compounds(frame)
    p_frame = percentage(s_frame)
    p_column = 'Percentage'
    boolean = False
    l_frame = sorted_df(p_frame, p_column, boolean)
    return l_frame


def main():
    path = os.path.abspath('all_files')
    folder_path = 'all_files/shared_flavors_and_compounds'
    create_folder(folder_path)
    json_file = path + '/' + 'data_after_qc.json'
    full_data = read_data(json_file)
    cluster_folder_path = os.path.abspath('all_files/plant_clusters')

    files = os.listdir(cluster_folder_path)
    # print(both_files)
    for each_file in files:
        data = read_csv_files(cluster_folder_path + '/' + each_file)
        print(each_file)
        m_frame = merge(data, full_data)
        print(m_frame.alias)
        print(m_frame.columns)
        shared_molecules_lists = molecules(m_frame)
        last_frame = process(shared_molecules_lists)
        print(last_frame)
        path_3= os.path.abspath('all_files/shared_flavors_and_compounds')
        path_and_name = path_3+'/'+each_file
        save_frame(last_frame,path_and_name)


if __name__ == '__main__':
    main()
