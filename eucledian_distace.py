"""
    Copyright (C) 2021  Y.Dogay [dogayyagmur@gmail.com]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from numpy.linalg import norm
import pandas as pd


def create_lists_for_new_frame(n_data):
    """
    create different lists and append values for ingredient_1,
    ingredient_2 and their eucledian distace difference,
    category, compounds that they contain and pubchem_ids
    Then create nestes list from those lists
    """
    i_1 = []
    i_2 = []
    normm = []
    category = []
    p_id_1 = []
    p_id_2 = []
    c_name_1 = []
    c_name_2 = []
    for num in range(0, 3):
        for item in range(len(n_data.alias)):
            print('------------')
            print(n_data['alias'][num], n_data['alias'][item])
            value_a = n_data.loc[num][4:].values
            value_b = n_data.loc[item][4:].values
            value = norm(value_a - value_b)
            catego = n_data['category_readable'][item]
            i_1.append(n_data['alias'][num])
            i_2.append(n_data['alias'][item])
            normm.append(value)
            p_id_1.append(n_data['pubchem_id'][num])
            p_id_2.append(n_data['pubchem_id'][item])
            c_name_1.append(n_data['common_name'][num])
            c_name_2.append(n_data['common_name'][item])
            category.append(catego)

    df_list = [i_1, i_2, normm, category, p_id_1, p_id_2, c_name_1, c_name_2]
    return df_list


def select_rows(distance_df, number):
    """
    sorting rows based on distance column
    and selecting the firth n th number of rows
    """
    most_df = pd.DataFrame(columns=distance_df.columns)
    for ing in distance_df.Ingredient_1.unique():
        frame = distance_df[distance_df['Ingredient_1'] == ing].sort_values(by='Distance')
        print(frame.iloc[:number, :])
        frame = frame.iloc[:number, :]
        most_df = most_df.append(frame)
    most_df.reset_index(drop=True, inplace=True)
    return most_df
