"""
    Copyright (C) 2021  Y.Dogay [dogayyagmur@gmail.com]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import os
import pandas as pd
import numpy as np
from correlation_plant_based import clean_set
from merge_csvs import read_csv_files
from molecules_dframe import read_data
from pearson_correlation import sorted_df


def drop_and_replace(data):
    """
    drop '[' and ']' from columns
    and split numbers buy comma (,)
    """
    data_2 = data.copy()
    columns = ['Shared_pubchem_id']
    for column in columns:
        #     df[column] = df[column].str.join(', ')
        # delete square brackets from columns
        data_2[column] = data_2[column].str.replace(r"\[", "", regex=True)
        data_2[column] = data_2[column].str.replace(r"\]", "", regex=True)
        data_2[column] = data_2[column].str.split(",")
    return data_2


def explode(data):
    """
    explode rows
    """
    data = data.explode('Shared_pubchem_id')
    data.reset_index(drop=True, inplace=True)
    data.rename(columns={'Shared_pubchem_id': 'pubchem_id'}, inplace=True)
    data = data[data['pubchem_id'] != '']
    return data


def choose_n_number(data_2):
    """
    putting plant based ingredient and
    meat categories in one frame
    """
    data_2 = data_2[data_2['Category'] == 'Plant_Based']
    data_2.reset_index(drop=True, inplace=True)
    data = pd.DataFrame(columns=data_2.columns)
    data_3 = data_2.copy()
    meats = ['beef', 'chicken', 'pork']
    for meat in meats:
        frame = data_3[data_3['Ingredient_1'] == meat].iloc[:, :]
        data = data.append(frame, ignore_index=True)
    data = data[['Ingredient_1', 'Ingredient_2', 'Shared_pubchem_id']]
    return data


def flavor_frame(data, data_2):
    """
    changing dtype
    creating list
    reseting index
    creating new columns
    """
    data['pubchem_id'] = data['pubchem_id'].astype(int)
    values = list(data.pubchem_id)
    data_2 = data_2[['pubchem_id', 'common_name', 'flavor_profile']]
    d_7 = data_2[data_2.pubchem_id.isin(values) == True]
    d_7.reset_index(drop=True, inplace=True)
    d_7 = d_7[d_7['flavor_profile'] != '']
    d_7.reset_index(drop=True, inplace=True)

    data.reset_index(drop=True, inplace=True)
    data['common_name'] = np.nan
    data['flavor_profile'] = np.nan
    # print(data)
    print(d_7)
    for i in range(len(d_7.pubchem_id)):
        for item in range(len(data.pubchem_id)):
            if d_7.pubchem_id[i] == data.pubchem_id[item]:
                print(d_7.pubchem_id[i], data.pubchem_id[item])
                data['common_name'].loc[item] = d_7['common_name'][i]
                data['flavor_profile'].loc[item] = d_7['flavor_profile'][i]
            else:
                continue
    return data


def create_list(data):
    """
    spliting row values with comma
    """
    columns = ['flavor_profile']
    for column in columns:
        data[column] = data[column].str.split(",")
    return data


def explode_flavor(data):
    """
    dropping duplicate rows
    and explode flavor column
    """
    data_5 = data.explode('flavor_profile')
    data_5.drop_duplicates(subset=['Ingredient_1', 'Ingredient_2',
                                   'flavor_profile'], keep=False, inplace=True)
    data_5.reset_index(drop=True, inplace=True)
    return data_5


def remove_space(string):
    """
    removing blank space
    """
    return string.replace(" ", "")


def save_frame(data_8, path, name):
    """
    shared flavors
    """
    new_df = pd.DataFrame(columns=['Ingredient_1', 'Ingredient_2', 'Shared_Flavors'])
    name_1 = []
    name_2 = []
    flavors = []
    number = []
    for meat in data_8.Ingredient_1.unique():
        frame = data_8[data_8['Ingredient_1'] == meat]
        ingredient = frame.Ingredient_2.unique()
        print(ingredient)

        for ing in ingredient:
            name_1.append(meat)
            name_2.append(ing)
            frame_2 = frame[frame['Ingredient_2'] == ing]
            flavor = frame_2['flavor_profile'].unique()
            print(frame_2['flavor_profile'].unique())
            flavors.append(flavor)
            number.append(len(flavor))
    #         print(frame_2)

    new_df['Ingredient_1'] = name_1
    new_df['Ingredient_2'] = name_2
    new_df['Shared_Flavors'] = flavors
    new_df['Number'] = number

    column_list = []
    for row in new_df.Shared_Flavors:
        row_list = []
        for item in row:
            print(item)
            item = remove_space(str(item))
            row_list.append(item)
        column_list.append(set(row_list))

    new_df['Shared_Flavors'] = column_list
    column = 'Number'
    boolean = False
    new_df = sorted_df(new_df, column, boolean)

    new_df.to_csv(path + '/' + "shared_flavors_" + name + '.csv')
    return new_df


def main():
    """
    calling functions
    """
    folder_path = os.path.abspath('all_files/shared_flavors_and_compounds')
    path_2 = os.path.abspath('all_files')
    list_of_files = os.listdir(folder_path)
    for each_file in list_of_files:
        if each_file.startswith('kernel') and each_file.endswith('.csv'):
            print(each_file)
            csv_name = each_file.split('.')[-2]
            file = folder_path + '/' + each_file
            file_2 = path_2 + '/' + 'molecules_after_qc.json'

            data_1 = read_csv_files(file)
            data_2 = read_data(file_2)
            data_1 = clean_set(data_1)
            data_3 = drop_and_replace(data_1)
            data_4 = choose_n_number(data_3)
            # print(data_4)
            data_5 = explode(data_4)
            # print(data_5)
            data_6 = flavor_frame(data_5, data_2)
            print(data_6)
            data_7 = create_list(data_6)
            print(data_7)
            data_8 = explode_flavor(data_7)
            print(data_8)
            data_9 = save_frame(data_8, folder_path, csv_name)
            print(data_9)
        else:
            continue


if __name__ == '__main__':
    main()
