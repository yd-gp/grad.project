"""
    Copyright (C) 2021  Y.Dogay [dogayyagmur@gmail.com]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import urllib.request
import json
import os
import ssl


def create_folder():
    """
    creating a folder
    """
    if os.path.exists('./FlavorDB_Json_Files'):
        pass
    else:
        os.mkdir('./FlavorDB_Json_Files')


# url='https://cosylab.iiitd.edu.in/flavordb/entities_json?id=148'


def flavordb_url(page):
    """
    url page
    """
    return "https://cosylab.iiitd.edu.in/flavordb/entities_json?id=" + str(page)


def hack_ssl():
    """
    ignores the certificate errors
    """
    ctx = ssl.create_default_context()
    ctx.check_hostname = False
    ctx.verify_mode = ssl.CERT_NONE
    return ctx


def open_url(url,page):
    """
    opens url
    """
    #     pages = range(0,1000)
    missing_id = []
    #     for page in pages:
    try:
        ctx = hack_ssl()
        html = urllib.request.urlopen(url, context=ctx).read()
        data = json.loads(html.decode('utf-8'))
        # the json file where the output must be stored
        path = os.path.abspath('FlavorDB_Json_Files')
        out_file = open(str(path) + "/" + str(page) + ".json", "w")
        json.dump(data, out_file)
        # return out_file.close()
    except urllib.error.HTTPError as e:
        if e.code == 404:  # if the JSON file is missing
            missing_id.append(page)


def main():
    """
    page range between 0 and 1001
    downloading all json files by calling function
    """
    create_folder()
    pages = range(0, 1001)
    for page in pages:
        url = flavordb_url(page)
        open_url(url, page)


if __name__ == '__main__':
    main()
