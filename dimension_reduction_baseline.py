"""
    Copyright (C) 2021  Y.Dogay [dogayyagmur@gmail.com]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import os
import warnings
import pandas as pd
from sklearn.manifold import TSNE, MDS
from sklearn.decomposition import KernelPCA
from merge_csvs import read_csv_files
from molecules_dframe import read_data
from merge_and_clean import create_folder

warnings.simplefilter(action='ignore', category=FutureWarning)


def pre_processing_for_mds(data, data_2):
    """
    merging datas to add alias and category to baselinedata
    """
    data_2 = data_2[['alias', 'category_readable']]
    merged_data = pd.merge(data_2, data, on="alias")
    return merged_data


def change_category_to_plant_base(data):
    """
    putting all plant-based ingredients under
    plant-based category
    """
    data_3 = data.copy()
    plant_based_categories = ['Gourd', 'Vegetable', 'Legume',
                              'Spice', 'Berry', 'Essential Oil',
                              'Nut', 'Herb', 'Fruit', 'Cabbage', 'Fungus',
                              'Plant Derivative', 'Vegetable Fruit',
                              'Vegetable Tuber', 'Vegetable Root',
                              'Flower', 'Cereal', 'Plant',
                              'Seed', 'Fruit Essence',
                              'Fruit Citrus', 'Vegetable Stem']
    name_1 = []
    for category in plant_based_categories:
        for i in range(len(data_3.category_readable)):
            if category == data_3['category_readable'][i]:
                name_1.append(data_3['alias'][i])
            else:
                continue

    name_2 = ['corn', 'sweetcorn', 'corn oil', 'soy milk', 'hummus', 'salad',
              'soy cream', 'kombu', 'irish moss', 'wakame', 'kelp', 'fruit salad',
              'syrup', 'hummus', 'salad', 'falafel', 'hibiscus tes', 'black tea',
              'green tea', 'soy milk', 'cofee', 'relish', 'couscous',
              'spirulina', 'ketchup', 'sugar',
              'white bread', 'pie crust', 'rye bread', 'potato bread', 'wholewheat bread',
              'multigrain bread', 'pasta', 'focaccia', 'bread', 'phyllo dough', 'wonton wrapper',
              'pita bread', 'vegetable juice', 'fruit juice', 'soy milk', 'arrack',
              'sherry', 'green tea', 'black tea', 'coffee', 'mate', 'popcorn']

    mylist = list(dict.fromkeys(name_2))
    # print(mylist)

    names = name_1 + mylist
    for name in names:
        data_3.loc[data_3.alias == name, 'category_readable'] = "Plant_Based"

    # print(data_3.category_readable.unique())

    # print(data_3.head())
    index = data_3[data_3['alias'] == 'honey'].index.values.tolist()
    data_3.loc[index, 'category_readable'] = 'Animal Product'
    return data_3


def make_three_main_categories(data):
    """
    three main categories, plant based, meat for beef chicken and pork and
    others for other ingredients
    the reason to to this is it will make visualization easy for seeing
    clusters, neighbours for beef, chicken and pork
    """
    data_4 = data.copy()
    for i in range(len(data_4.category_readable)):
        if data_4.loc[i, 'category_readable'] != 'Plant_Based':
            data_4.loc[i, 'category_readable'] = data_4.loc[i, 'category_readable'].replace(
                data_4.loc[i, 'category_readable'], 'Other')
    data_4.set_index(['alias'], inplace=True)
    meats = ['beef', 'chicken', 'pork']
    for meat in meats:
        data_4.loc[meat, 'category_readable'] = 'Meat'
    data_4.reset_index(inplace=True)
    return data_4


def category_colors(data_4, column):
    """
    assigning color column based on ingredient category
    """
    colors = {'Meat': '#D55E00', 'Plant_Based': '#0072B2', 'Other': '#000000'}
    data_4["color"] = data_4[column].apply(lambda c: colors[c])
    return data_4


def mds(data_4, data_5, path, name):
    """
    applying MDS on data
    saving result as json
    """
    mds_1 = MDS(2, random_state=0, dissimilarity='euclidean')
    mds_results = mds_1.fit_transform(data_5)

    mds_results = pd.DataFrame(mds_results, columns=['dimension1', 'dimension2'])
    mds_results['alias'], mds_results['category_readable'], \
    mds_results['color'] = [data_4['alias'],
                            data_4['category_readable'], data_4['color']]

    mds_results.to_json(path + '/' + str(name))
    return mds_results


def kernel_pca_reduction(data_4, data_5, path, name):
    """
    applying KPCA on data
    saving result as json
    """
    kpca = KernelPCA(n_components=2, kernel="cosine", fit_inverse_transform=True, random_state=42)
    kpca_results = kpca.fit_transform(data_5)
    kpca_results = pd.DataFrame(kpca_results, columns=['dimension1', 'dimension2'])
    kpca_results['alias'], kpca_results['category_readable'], \
    kpca_results['color'] = [data_4['alias'], data_4['category_readable'], data_4['color']]

    kpca_results.to_json(path + '/' + str(name))
    return kpca_results


def t_sne_reduction(data_4, data_5, path, name):
    """
    applying tsne on data
    saving result as json
    """
    results = []
    for num in [10, 20, 30, 40, 50]:
        tsne = TSNE(2, random_state=42, perplexity=num, metric='cosine')
        tsne_results = tsne.fit_transform(data_5)
        tsne_results = pd.DataFrame(tsne_results, columns=['dimension1', 'dimension2'])
        tsne_results['alias'], tsne_results['category_readable'], \
        tsne_results['color'] = [data_4['alias'], data_4['category_readable'], data_4['color']]

        tsne_results.to_json(str(path) + '/' + name + str(num) + '.json')
        print(tsne_results)
        results.append(tsne_results)
    return results


def main():
    """
    calling functions to provide mds, tsne and kpca on data
    providing json file names
    """
    path = os.path.abspath('all_files/baseline')
    path_2 = os.path.abspath('all_files')
    file = path + '/' + "encoding.csv"
    file_2 = path_2 + '/' + "data_after_qc.json"
    data_1 = read_csv_files(file)
    data_2 = read_data(file_2)
    folder_name = 'all_files/baseline/dimension_reduction'
    create_folder(folder_name)
    # print(data_1)
    # print(data_2)
    merged_data = pre_processing_for_mds(data_1, data_2)
    changed_data = change_category_to_plant_base(merged_data)
    # print(merged_data)
    # print(changed_data)
    main_category_data = make_three_main_categories(changed_data)
    # print(main_category_data[main_category_data['alias']=='beef'])
    colors_frame = category_colors(main_category_data, column='category_readable')
    # print(colors_frame)
    data_5 = colors_frame.iloc[:, 2:-1]
    result_folder_path = os.path.abspath('all_files/baseline/dimension_reduction')
    name = 'mds_result_baseline.json'
    mds(colors_frame, data_5, result_folder_path, name)
    name = 'kernel_pca_result_baseline.json'
    kernel_pca_reduction(colors_frame, data_5, result_folder_path, name)
    name = 'tsne_result_baseline_perplexity_'
    t_sne_reduction(colors_frame, data_5, result_folder_path, name)


if __name__ == '__main__':
    main()
