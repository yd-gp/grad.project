"""
    Copyright (C) 2021  Y.Dogay [dogayyagmur@gmail.com]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
from collections import OrderedDict
from math import log, sqrt
import numpy as np
from bokeh.io import save
from bokeh.models import Tabs, Panel
from bokeh.plotting import figure, output_file, show
from merge_csvs import read_csv_files
from merge_and_clean import create_folder


def plot_pre_processing(data):
    """
    preprocess for plotting;
    making 2 categories (Plant based, Not plant based)
    out of three main categories (Plant based, meat, other)
    """
    data = data[['Ingredient_1', 'Ingredient_2', 'Distance', 'Category',
                 'Number_of_shared_compounds', 'Set_number_of_compounds', 'Percentage']]
    categories = ['Meat', 'Other']
    for category in categories:
        data.loc[data.Category == category, 'Category'] = 'Not_Plant_Based'

    general_list = []
    for i in data['Category']:
        if i != 'Plant_Based':
            general_list.append('Not_Plant_based')
        elif i == 'Plant_Based':
            general_list.append('Plant_based')
    data['General_Category'] = general_list
    return data


def plot(data, name):
    """
    plotting distance and % of shared compounds
    """
    plots = []
    for item in data.Ingredient_1.unique():
        df = data[data['Ingredient_1'] == item]
        df.reset_index(drop=True, inplace=True)
        info_color = OrderedDict([
            ("Distance", "#0d3362"),
            ("Shared Compounds %", "#c64737"),
        ])
        width = 650
        height =650
        inner_radius = 100
        outer_radius = 300 - 10
        minr = sqrt(log(.1 * 1E4))
        maxr = sqrt(log(1000 * 1E4))
        a = (outer_radius - inner_radius) / (minr - maxr)
        b = inner_radius - a * maxr

        def rad(mic):
            return a * np.sqrt(np.log(mic * 1E4)) + b

        big_angle = 2.0 * np.pi / (len(df) + 3)
        small_angle = big_angle / 7

        p = figure(plot_width=width, plot_height=height, title=item.upper(),
                   x_axis_type=None, y_axis_type=None,
                   x_range=(-420, 420), y_range=(-420, 420),
                   min_border=0)

        p.xgrid.grid_line_color = None
        p.ygrid.grid_line_color = None
        category_color = OrderedDict([
            ("Not_Plant_based", "#aeaeb8"),
            ("Plant_based", "#e69584"),
        ])
        colors = [category_color[category] for category in df.General_Category]
        # print(color)
        # annular wedges
        angles = np.pi / 2 - big_angle / 2 - df.index.to_series() * big_angle
        p.annular_wedge(
            0, 0, inner_radius, outer_radius, -big_angle + angles, angles, color=colors,
        )
        # print('inner_radius', inner_radius)
        # print('outer_radius', outer_radius)
        # print('big angle', big_angle)
        # print('angles', angles)
        # print('mirt', minr)
        # print('mirt', maxr)
        # print(a)
        # print(b)
        # print('----------')

        # small wedges
        p.annular_wedge(0, 0, inner_radius, rad(df.Distance),
                        -big_angle + angles + 5 * small_angle,
                        -big_angle + angles + 6 * small_angle,
                        color=info_color['Distance'])
        p.annular_wedge(0, 0, inner_radius, rad(df.Percentage),
                        -big_angle + angles + 3 * small_angle,
                        -big_angle + angles + 4 * small_angle,
                        color=info_color["Shared Compounds %"])

        # circular axes and lables
        labels = np.power(4.0, np.arange(-1, 6))
        radii = a * np.sqrt(np.log(labels * 1E4)) + b
        p.circle(0, 0, radius=radii, fill_color=None, line_color="white")
        p.text(0, radii[:-1], [str(r) for r in labels[:-1]],
               text_font_size="9px", text_align="right", text_baseline="middle")

        # radial axes
        p.annular_wedge(0, 0, inner_radius - 10, outer_radius + 10,
                        -big_angle + angles, -big_angle + angles, color="black")

        xr = (radii[0] + 33) * np.cos(np.array(-big_angle / 2 + angles))
        yr = (radii[0] + 33) * np.sin(np.array(-big_angle / 2 + angles))
        label_angle = np.array(-big_angle / 2 + angles)
        label_angle[label_angle < -np.pi / 2] += np.pi
        p.text(xr, yr, df.Ingredient_2, angle=label_angle,
               text_font_size="12px", text_align="center", text_baseline="middle")

        p.circle([-40, -40], [-370, -390], color=list(category_color.values()), radius=5)
        p.text([-30, -30], [-370, -390], text=[ct for ct in category_color.keys()],
               text_font_size="9px", text_align="left", text_baseline="middle")

        p.rect([-65, -65, -65], [18, 0, -18], width=25, height=13,
               color=list(info_color.values()))
        p.text([-45, -45, -45], [18, 0, -18], text=list(info_color),
               text_font_size="9px", text_align="left", text_baseline="middle")

        tab = Panel(child=p, title=item)
        plots.append(tab)

    tabs_object = Tabs(tabs=plots)
    path = os.path.abspath('all_files/wheel_plots')
    output_file(str(path) + '/' + str(name) + '.html')
    save(tabs_object)
    show(tabs_object)


def main():
    """
    create folder
    path to distance csvs
    calling functions to plot wheel plots
    """
    folder_name = 'all_files/wheel_plots'
    create_folder(folder_name)
    eucledian_folder_path = os.path.abspath('all_files/distance')
    list_of_embedding_files = os.listdir(eucledian_folder_path)
    for each_file in list_of_embedding_files:
        if each_file.endswith('.csv'):
            print(each_file)
            file_name = each_file.split('.')[-2]
            data = read_csv_files(eucledian_folder_path + '/' + each_file)
            data = plot_pre_processing(data)
            # print(data)
            # print(data.Category.unique())
            # print(data.General_Category.unique())
            name = file_name
            plot(data, name)


if __name__ == '__main__':
    main()
