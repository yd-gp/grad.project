"""
    Copyright (C) 2021  Y.Dogay [dogayyagmur@gmail.com]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import ast
import os
import shutil
import pandas as pd


def read_csv_files(file):
    """
    read csv files
    """
    data_1 = pd.read_csv(file)
    return data_1


def get_row_keys_values(data):
    """
    change type from string to dictionary
    """
    for i in range(len(data.n)):
        data['n'][i] = ast.literal_eval(data['n'][i])
    return data


def lists_of_key_values(data):
    """
    get key values and put them into related lists
    """
    idd = []
    name = []
    label = []
    flavor = []
    key_1 = 'FLAVOR_PROFILE'
    for i in range(len(data.n)):
        idd.append(data.n[i]['id'])
        label.append(data.n[i]['labels'])
        name.append(data.n[i]['properties']['NAME'])
        if key_1 in data.n[i]['properties'].keys():
            flavor.append(data.n[i]['properties'][key_1])
        else:
            pass
    return idd, name, label, flavor


def crate_columns(data, idd, name, label):
    """
    create new columns for data frame
    """
    data['nodeId'] = idd
    data['name'] = name
    data['label'] = label
    return data


def merge_data_frames(data1, data2):
    """
    merge two data frame based on nodeId column
    """
    merged_frame = pd.merge(data1, data2, on="nodeId")
    return merged_frame


def save_file(data, path, filename):
    """
    save file as csv
    """
    data.to_csv(path + '/' + filename)


def create_embedding_folder():
    """
    create a folder
    """
    if os.path.exists('./all_files/gdb/embedding_folder'):
        pass
    else:
        os.mkdir('./all_files/gdb/embedding_folder')


def create_merged_folder():
    """
    create a folder
    """
    if os.path.exists('./all_files/gdb/merged_folder'):
        pass
    else:
        os.mkdir('./all_files/gdb/merged_folder')


def main():
    """
    path to graph csv,
    find and remove them to a folder
    call all functions to merge and preprocess frames
    """
    create_embedding_folder()
    create_merged_folder()
    path_2 = os.path.abspath('all_files/gdb')

    list_of_files = os.listdir(path_2)  # list of files in the current directory
    for each_file in list_of_files:
        if each_file.startswith('embedding') and each_file.endswith('.csv'):
            print(each_file)
            path_3 = os.path.abspath('all_files/gdb/embedding_folder')

            shutil.move(path_2 + '/' + each_file, path_3 + '/' + each_file)

    embedding_csv_path = os.path.abspath('all_files/gdb/embedding_folder')
    list_of_embedding_files = os.listdir(embedding_csv_path)
    for each_file in list_of_embedding_files:
        if each_file.startswith('embedding') and each_file.endswith('.csv'):
            # print(each_file)
            csv_name = each_file.split('.')[-2]
            file_name = read_csv_files(embedding_csv_path + '/' + each_file)

            print(file_name)
            path = os.path.abspath('all_files/gdb')
            csv_file_1 = path + '/' + 'GDB.csv'
            gdb = read_csv_files(csv_file_1)
            data = get_row_keys_values(gdb)
            print(data)
            lists = lists_of_key_values(gdb)
            print(lists[0])
            print(lists[1])
            print(lists[2])
            idd = lists[0]
            name = lists[1]
            label = lists[2]
            data_2 = crate_columns(data, idd, name, label)
            print(data_2)
            merged_data = merge_data_frames(data_2, file_name)
            print(merged_data)
            path_4 = 'all_files/gdb/merged_folder/'
            filename = 'gdb_and_' + str(csv_name) + '.csv'
            save_file(merged_data, path_4, filename)
            print(merged_data.columns)


if __name__ == '__main__':
    main()
