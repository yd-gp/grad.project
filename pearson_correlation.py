"""
    Copyright (C) 2021  Y.Dogay [dogayyagmur@gmail.com]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import itertools
import os
import pandas as pd
from scipy import stats
from merge_csvs import read_csv_files
from molecules_dframe import read_data
from dimension_reduction_baseline import change_category_to_plant_base, make_three_main_categories
from merge_and_clean import create_folder


def merge_frames(data, data_2):
    """
    merging datas to add alias and category to baselinedata
    """
    data_2 = data_2[['category_readable', 'alias', 'pubchem_id', 'common_name']]
    merged_data = pd.merge(data_2, data, on="alias")
    return merged_data


def reindex(data):
    """
    reindex the dataframe based on meat categories
    putting meat category ingredients in to the first places of the frame
    """
    m_category = list(data[data['category_readable'] == 'Meat'].index)
    o_category = list(data[data['category_readable'] != 'Meat'].index)
    c_category = m_category + o_category
    data = data.reindex(c_category)
    data.reset_index(drop=True, inplace=True)
    return data


def create_lists_for_new_data(data):
    """
    create different lists and append values for ingredient_1,
    ingredient_2 and their p value, coefficient correlation,
    category, compounds that they contain and pubchem_ids
    Then create nested list from those lists
    """
    i_1 = []
    i_2 = []
    p_value = []
    coefficient = []
    category = []
    p_id_1 = []
    p_id_2 = []
    c_name_1 = []
    c_name_2 = []
    for num in range(0, 3):
        for item in range(len(data.alias)):
            print('------------')
            print(data['alias'][num], data['alias'][item])
            #         print(data.loc[i][1:].values)
            #         print(meat.loc[ii][1:].values)
            #         print(meat.iloc[i:ii,12:].values)
            #         print(meat.iloc[i:ii,12:].values)
            value_a = data.loc[num][4:].values
            value_b = data.loc[item][4:].values
            value_a = list(value_a)
            value_b = list(value_b)
            #         print('norm')
            #         print(norm(a-b))
            p_number = stats.pearsonr(value_a, value_b)[1]
            coef = stats.pearsonr(value_a, value_b)[0]
            p_value.append(p_number)
            coefficient.append(coef)

            catego = data['category_readable'][item]
            #         key=data['alias'][i],data['alias'][ii]
            #         listt.append(list((data['alias'][i],data['alias'][ii],value)))
            i_1.append(data['alias'][num])
            i_2.append(data['alias'][item])
            p_id_1.append(data['pubchem_id'][num])
            p_id_2.append(data['pubchem_id'][item])
            c_name_1.append(data['common_name'][num])
            c_name_2.append(data['common_name'][item])
            category.append(catego)
    df_list = [i_1, i_2, p_value, coefficient, category, p_id_1, p_id_2, c_name_1, c_name_2]
    return df_list


def create_data_frame(df_list, columns):
    """
    create new dataframe with using nested list
    """
    distance_df = pd.DataFrame((_ for _ in itertools.zip_longest(*df_list)), columns=columns)
    distance_df = distance_df[distance_df['Ingredient_1'] != distance_df['Ingredient_2']]
    return distance_df


def p_less_than_05(distance_df):
    """
    getting read of ingredients whose p_values are more than 0.05
    """
    p_df = distance_df[distance_df['P_value'] <= 0.05]
    p_df.reset_index(drop=True, inplace=True)
    return p_df


def s_compounds(p_df):
    """
    creating new columns based on shared compounds and number of shared compounds
    """
    shared_compound_id = []
    shared_compound_name = []
    number_s_compouds = []
    set_of_two_compound_lists = []
    for i in range(len(p_df.I_1_Pubchem_id)):
        shared_compound_id.append(list(set(p_df.I_1_Pubchem_id[i])
                                       & set(p_df.I_2_Pubchem_id[i])))
        shared_compound_name.append(list(set(p_df.I_1_Common_name[i])
                                         & set(p_df.I_2_Common_name[i])))
        number_s_compouds.append(len(list(set(p_df.I_1_Common_name[i])
                                          & set(p_df.I_2_Common_name[i]))))
        set_of_two_compound_lists.append(len(set(p_df.I_1_Common_name[i]
                                                 + p_df.I_2_Common_name[i])))
    p_df2 = p_df.assign(Shared_pubchem_id=shared_compound_id,
                        Shared_compound_name=shared_compound_name,
                        Number_of_shared_compounds=number_s_compouds,
                        Set_number_of_compounds=set_of_two_compound_lists)
    return p_df2


def sorted_df(p_df2, column, boolean):
    """
    sorting dataframe based on a column (p_value)
    """
    p_df3 = pd.DataFrame(columns=p_df2.columns)
    for ing in p_df2.Ingredient_1.unique():
        frame = p_df2[p_df2['Ingredient_1'] == ing].sort_values(by=column, ascending=boolean)
        p_df3 = p_df3.append(frame)
    p_df3.reset_index(drop=True, inplace=True)
    return p_df3


def shared_compounds_percentage(ingredient_1, ingredient_2):
    """
    calculating percentage (%) of compounds that shared between two ingredients
    """
    try:
        compounds_percentage = (ingredient_1 / ingredient_2) * 100
    except ZeroDivisionError:
        compounds_percentage = 0
    return compounds_percentage


def percentage(data):
    """
    assigning calculated percentages to frame as a new column
    """
    p_list = [shared_compounds_percentage(i, j) for i, j in
              zip(data['Number_of_shared_compounds'], data['Set_number_of_compounds'])]
    data = data.assign(Percentage=p_list)
    return data


def save_frame(frame, name):
    """
    save frame in a folder
    """
    frame.to_csv(name)


def main():
    """
    paths to files
    calling functions to run the script to calculate correlation
    """
    path = os.path.abspath('all_files')
    path_2 = os.path.abspath('all_files/baseline')
    json_file = path + '/' + 'data_after_qc.json'
    baseline_data = read_csv_files(path_2 + '/' + 'encoding.csv')
    full_data = read_data(json_file)
    merged_frame = merge_frames(baseline_data, full_data)
    # print(merged_frame)
    changed_category_frame = change_category_to_plant_base(merged_frame)
    main_category_frame = make_three_main_categories(changed_category_frame)
    print(main_category_frame[main_category_frame['category_readable'] == 'Meat'])
    # print(main_category_frame.iloc[:,2:])
    print(main_category_frame.iloc[:, 3:])
    print(main_category_frame.iloc[:, 4:])
    print(main_category_frame.columns)
    re_index_frame = reindex(main_category_frame)
    print(re_index_frame)
    lists = create_lists_for_new_data(re_index_frame)
    columns = ['Ingredient_1', 'Ingredient_2', 'P_value',
               'Coefficient', 'Category', 'I_1_Pubchem_id',
               'I_2_Pubchem_id', 'I_1_Common_name', 'I_2_Common_name']
    new_frame = create_data_frame(lists, columns)
    p_less_05 = p_less_than_05(new_frame)
    print(p_less_05)
    p_column = 'P_value'
    boolean = True
    sorted_frame = sorted_df(new_frame, p_column, boolean)
    c_frame = s_compounds(sorted_frame)

    print(c_frame)
    folder_name_path = 'all_files/baseline/correlation/'
    create_folder(folder_name_path)
    last_frame = percentage(c_frame)
    path_3 = os.path.abspath('all_files/baseline/correlation/')
    path_and_name = os.path.abspath(path_3 + '/' + 'pearson_correlation' + '.csv')
    save_frame(last_frame, path_and_name)
    print(last_frame)


if __name__ == '__main__':
    main()
