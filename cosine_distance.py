"""
    Copyright (C) 2021  Y.Dogay [dogayyagmur@gmail.com]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import os
from scipy.spatial.distance import cdist
from distance_for_all import baseline, node2vec
from merge_and_clean import create_folder
from molecules_dframe import read_data


def create_lists(data_5):
    """
    create different lists and append values for ingredient_1,
    ingredient_2 and calculating their cosine distace difference,
    category, compounds that they contain and pubchem_ids
    Then create nestes list from those lists
    """
    n_data = data_5.copy()
    i_1 = []
    i_2 = []
    normm = []
    category = []
    p_id_1 = []
    p_id_2 = []
    c_name_1 = []
    c_name_2 = []
    for num in range(0, 3):
        for item in range(len(n_data.alias)):
            print('------------')
            print(n_data['alias'][num], n_data['alias'][item])
            value_a = n_data.iloc[num:num + 1, 4:].values
            value_b = n_data.iloc[item:item + 1, 4:].values
            value = round(float(cdist(value_a, value_b, 'cosine')), 2)
            catego = n_data['category_readable'][item]
            i_1.append(n_data['alias'][num])
            i_2.append(n_data['alias'][item])
            normm.append(value)
            p_id_1.append(n_data['pubchem_id'][num])
            p_id_2.append(n_data['pubchem_id'][item])
            c_name_1.append(n_data['common_name'][num])
            c_name_2.append(n_data['common_name'][item])
            category.append(catego)

    df_list = [i_1, i_2, normm, category, p_id_1, p_id_2, c_name_1, c_name_2]
    return df_list


def main():
    """
    path to baseline and graph data
    calling functions to calculate cosine distance
    """
    path = os.path.abspath('all_files')
    folder_name_path = 'all_files/distance'
    create_folder(folder_name_path)
    json_file = path + '/' + 'data_after_qc.json'
    full_data = read_data(json_file)

    baseline_path = os.path.abspath('all_files/baseline')
    node2vec_path = os.path.abspath('all_files/gdb/merged_folder')
    baseline_files = os.listdir(baseline_path)
    node2vec_files = os.listdir(node2vec_path)
    both_files = baseline_files + node2vec_files
    # print(both_files)
    for each_file in both_files:
        # print(each_file)
        if each_file.endswith('.csv') and each_file.startswith('enco'):
            print(each_file)
            name = 'c_distance_baseline'
            baseline(each_file, baseline_path, full_data, create_lists, name)

        elif each_file.endswith('.csv') and each_file.startswith('gdb'):
            name = 'c_distance_'
            node2vec(each_file, node2vec_path, full_data, create_lists, name)


if __name__ == '__main__':
    main()
