"""
    Copyright (C) 2021  Y.Dogay [dogayyagmur@gmail.com]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
import time
import pandas as pd
from bokeh.models import ColumnDataSource, HoverTool, Title
from bokeh.plotting import figure, output_file, show
import matplotlib.pyplot as plt
from sklearn.metrics import silhouette_score
from sklearn.cluster import KMeans
from molecules_dframe import read_data
from pearson_correlation import create_folder


def elbow(data):
    """
    plotting elbow method to find suitable cluster number
    """
    ks_range = range(1, 10)
    inertias = []
    for k_num in ks_range:
        model = KMeans(n_clusters=k_num, random_state=42)
        model.fit(data.iloc[:, :2].values)
        inertias.append(model.inertia_)
    plt.plot(ks_range, inertias, '-o', color='black')
    plt.xlabel('number of clusters, k')
    plt.ylabel('inertia')
    plt.xticks(ks_range)
    plt.show()


def s_score(data):
    """
    calculating silhouette score to find suitable cluster number
    """
    value_x = data.iloc[:, :2]
    range_n_clusters = [2, 3, 4, 5, 6, 7, 8, 9]
    for n_clusters in range_n_clusters:
        clusterer = KMeans(n_clusters=n_clusters, random_state=42)
        preds = clusterer.fit_predict(value_x)
        # centers = clusterer.cluster_centers_

        score = silhouette_score(value_x, preds)
        print("For n_clusters = {}, silhouette score is {})".format(n_clusters, round(score, 2)))


def job_1(each_file, path):
    print(each_file)
    csv_name = each_file.split('.')[-2]
    print(csv_name)
    result_data = read_data(path + '/' + each_file)
    elbow(result_data)
    s_score(result_data)
    return result_data, csv_name


def k_means(data, cluster_number):
    """
    applying k means clustering
    """
    x_value = data.iloc[:, :2]
    kmeans = KMeans(n_clusters=cluster_number, random_state=42)
    labels = kmeans.fit_predict(x_value)
    data['cluster_label'] = labels
    return data


def cluster_plot(data, plot_title, plot_path, name_1, alpha):
    """
    plotting clusters
    """
    colors = {7: '#CC79A7',
              6: '#F0E442',
              5: '#56B4E9',
              4: '#009E73',
              3: '#E69F00',
              2: '#0072B2',
              1: '#D55E00',
              0: '#000000'}
    data["cluster_color"] = data['cluster_label'].apply(lambda c: colors[c])

    source = ColumnDataSource(
        data=dict(x=data.dimension1, y=data.dimension2, desc=data.alias,
                  category=data.category_readable,
                  coluor=data.color, cluster_label=data.cluster_label,
                  label_color=data.cluster_color))
    hover = HoverTool(tooltips=[
        ("category", "@category"),
        ("(x,y)", "(@x, @y)"),
        ('name', '@desc'),
    ])
    plot = figure(plot_width=400, plot_height=400, tools=[hover, 'pan',
                                                          'wheel_zoom', 'box_zoom',
                                                          'zoom_out', 'save'],
                  x_axis_label="Dimension 1",
                  y_axis_label="Dimension 2", background_fill_color="white",
                  background_fill_alpha=0.1)

    plot.circle(x='x', y='y', color='label_color', fill_alpha=alpha, line_alpha=2,
                line_width=1, source=source)
    plot.add_layout(Title(text=str(plot_title), text_font_size="9pt", align="center",
                          text_line_height=0.5),
                    'above')
    plot.add_layout(Title(text="K-means Clustering Analysis of", text_font_size="9pt",
                          align="center",
                          text_line_height=0.5), 'above')
    output_file(str(plot_path) + '/' + str(name_1) + '.html')
    show(plot)


def get_plant_based_ingredients(data, cluster_label):
    """
    getting plant based ingredients that clusteres within
    same cluster as beef, chicken and pork
    """
    data_2 = data[data['cluster_label'] == cluster_label]
    p_data = data_2[data_2['category_readable'] == 'Plant_Based']
    p_data.reset_index(drop=True, inplace=True)
    return p_data


def append_meats(data, plant_data):
    """
    adding meat categories to plant frame
    """
    beef = data[data['alias'] == 'beef']
    chicken = data[data['alias'] == 'chicken']
    pork = data[data['alias'] == 'pork']
    frames = [beef, chicken, pork, plant_data]
    data_2 = pd.DataFrame(columns=data.columns)
    for i in frames:
        data_2 = data_2.append(i, ignore_index=True)
    data_2.reset_index(drop=True, inplace=True)
    return data_2


def job_2(result_data, c_number, csv_name, title):
    k_means_data = k_means(result_data, cluster_number=c_number)
    print(k_means_data[k_means_data['alias'] == 'beef'])
    print(k_means_data[k_means_data['alias'] == 'pork'])
    print(k_means_data[k_means_data['alias'] == 'chicken'])
    print(k_means_data[k_means_data['alias'] == 'mushroom'])
    plot_path = os.path.abspath('all_files/cluster_plot')
    cluster_plot(k_means_data, plot_title=title, plot_path=plot_path, name_1=csv_name, alpha=0.3)
    return k_means_data


def label(data):
    """
    finding label number of meat category
    """
    labels = list(data[data['alias'] == 'chicken']['cluster_label'])
    return labels[0]


def baseline(each_file, path):
    """
    calling functions for baseline data,
    assigning cluster numbers
    if you rerun make sure that you inspect elbow and shiolette methods
    and changed the cluster number inside functions
    """
    if each_file.endswith('.json') and each_file.startswith('mds_result_baseline'):
        print(each_file)
        job = job_1(each_file, path)
        print('Sleeping for 3 min to decide the nuymber of clusters')
        time.sleep(180)
        print('Enter Cluster_number_for_mds_result_baseline')
        job_2(job[0], int(input()), job[1], title='Baseline Data')

    elif each_file.endswith('.json') and each_file.startswith('tsne_result_baseline'):
        json_name = each_file.split('.')[-2]
        p_num_name = json_name.split('_')[-1]
        # do(each_file, 2, baseline_path)
        # print(p_num_name)
        if p_num_name == '10':
            print('----')
            print(each_file)
            job = job_1(each_file, path)
            print('Sleeping for 3 min to decide the nuymber of clusters')
            time.sleep(180)
            print('Enter Cluster_number_for_tsne_result_baseline_10')
            job_2(job[0], int(input()), job[1], title='Baseline Data')
        elif p_num_name == '50':
            print('----')
            print(each_file)
            job = job_1(each_file, path)
            print('Sleeping for 3 min to decide the nuymber of clusters')
            time.sleep(180)
            print('Enter Cluster_number_for_tsne_result_baseline_50')
            job_2(job[0], int(input()), job[1], title='Baseline Data')
        else:
            print('---')
            job = job_1(each_file, path)
            print('Sleeping for 3 min to decide the nuymber of clusters')
            time.sleep(180)
            print('Enter Cluster_number_for_tsne_result_baseline_else')
            job_2(job[0], int(input()), job[1], title='Baseline Data')
    elif each_file.endswith('.json') and each_file.startswith('kernel_pca_result_baseline'):
        # do_job(each_file, 3, path, title='Baseline Data')
        job = job_1(each_file, path)
        print('Sleeping for 3 min to decide the nuymber of clusters')
        time.sleep(180)
        print('Enter Cluster_number_for_kernel_pca_result_baseline')
        d_r_data = job_2(job[0], int(input()), job[1], title='Baseline Data')
        labels = label(d_r_data)
        print('labelsssss', labels)
        p_data = get_plant_based_ingredients(d_r_data, labels)
        print(p_data)
        ff_data = append_meats(d_r_data, p_data)
        print('-----')
        print(ff_data)
        print(ff_data.columns)
        print('f_data')
        print(ff_data)
        list_p_ingredients = list(ff_data.alias)
        name = each_file.split('.')[-2]
        csv_path = os.path.abspath('all_files/plant_clusters')
        ff_data.to_csv(csv_path + '/' + name + '.csv')
        print(list_p_ingredients)


def graph_data_p12(each_file, path):
    """
    calling functions for graph p12 data
    assigning cluster numbers
    if you rerun make sure that you inspect elbow and shiolette methods
    and changed the cluster number inside functions
    """
    if each_file.endswith('p12.json') and each_file.startswith('mds_result_gdb'):
        print(each_file)
        job = job_1(each_file, path)
        print('Sleeping for 3 min to decide the nuymber of clusters')
        time.sleep(180)
        print('Enter Cluster_number_for_mds_result_gdb_p12')
        job_2(job[0], int(input()), job[1], title='Graph Data')
    elif each_file.endswith('p12.json') and each_file.startswith('kernel_pca_result_gdb'):
        print(each_file)
        job = job_1(each_file, path)
        print('Sleeping for 3 min to decide the nuymber of clusters')
        time.sleep(180)
        print('Enter Cluster_number_for_kernel_pca_result_gdb_p12')
        job_2(job[0], int(input()), job[1], title='Graph Data')
    elif each_file.endswith('.json') and \
            each_file.startswith('tsne_result_gdb_and_embedding_128d_p12'):
        json_name = each_file.split('.')[-2]
        p_num_name = json_name.split('_')[-1]
        # do(each_file, 2, baseline_path)
        # print(p_num_name)
        if p_num_name == '10':
            print('----')
            print(each_file)
            job = job_1(each_file, path)
            print('Sleeping for 3 min to decide the nuymber of clusters')
            time.sleep(180)
            print('Enter Cluster_number_for_tsne_result_gdb_p12_10')
            job_2(job[0], int(input()), job[1], title='Graph Data')
        else:
            job = job_1(each_file, path)
            print('Sleeping for 3 min to decide the nuymber of clusters')
            time.sleep(180)
            print('Enter Cluster_number_for_tsne_result_gdb_p12_else')
            job_2(job[0], int(input()), job[1], title='Graph Data')


def graph_data_p105(each_file, path):
    """
    calling functions for graph p105 data,
    assigning cluster numbers
    """
    if each_file.endswith('p105.json') and each_file.startswith('mds_result_gdb'):
        print(each_file)
        job = job_1(each_file, path)
        print('Sleeping for 3 min to decide the nuymber of clusters')
        time.sleep(180)
        print('Enter Cluster_number_for_mds_result_gdb_p105_else')
        job_2(job[0], int(input()), job[1], title='Graph Data')
    elif each_file.endswith('p105.json') and each_file.startswith('kernel_pca_result_gdb'):
        print(each_file)
        job = job_1(each_file, path)
        print('Sleeping for 3 min to decide the nuymber of clusters')
        time.sleep(180)
        print('Enter Cluster_number_for_kernel_pca_result_gdb_p105')
        d_r_data = job_2(job[0], int(input()), job[1], title='Graph Data')
        labels = label(d_r_data)
        print('labelsssss', labels)
        p_data = get_plant_based_ingredients(d_r_data, labels)
        print(p_data)
        ff_data = append_meats(d_r_data, p_data)
        csv_path = os.path.abspath('all_files/plant_clusters')
        name = each_file.split('.')[-2]
        ff_data.to_csv(csv_path + '/' + name + '.csv')
        list_p_ingredients = list(ff_data.alias)
        print(list_p_ingredients)
        return list_p_ingredients
    elif each_file.endswith('.json') and \
            each_file.startswith('tsne_result_gdb_and_embedding_128d_p105'):
        # json_name = each_file.split('.')[-2]
        # p_num_name = json_name.split('_')[-1]
        job = job_1(each_file, path)
        print('Sleeping for 3 min to decide the nuymber of clusters')
        time.sleep(180)
        print('Enter Cluster_number_for_tsne_result_gdb_p105')
        job_2(job[0], int(input()), job[1], title='Graph Data')


def main():
    """
    creating a folder
    calling functions to plot clustered data by k means algorithm
    """
    # path = os.path.abspath('all_files')
    folder_path = 'all_files/cluster_plot'
    folder_path_2 = 'all_files/plant_clusters'
    create_folder(folder_path)
    create_folder(folder_path_2)
    # json_file = path + '/' + 'data_after_qc.json'
    # full_data = read_data(json_file)

    baseline_path = os.path.abspath('all_files/baseline/dimension_reduction')
    node2vec_path = os.path.abspath('all_files/gdb/dimension_reduction')
    baseline_files = os.listdir(baseline_path)
    node2vec_files = os.listdir(node2vec_path)
    both_files = baseline_files + node2vec_files
    # print(both_files)
    for each_file in both_files:
        baseline(each_file, baseline_path)
        graph_data_p12(each_file, node2vec_path)
        graph_data_p105(each_file, node2vec_path)


if __name__ == '__main__':
    main()
